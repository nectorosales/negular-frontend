export const environment = {
  production: true,
  apiEndpoint: 'https://negular.com:3443/api/',
  apiEndpointPublic: 'https://negular.com:3443/public/',
  jitsiMeet: 'https://negular.com:8080/',
  apiJitsiMeet: 'negular.com:8080',
  domain: 'https://negular.com/'
};
