import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {SnackBarService} from '../services/shared/snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class AuthHelperService {
  constructor(private http: HttpClient, private snackService: SnackBarService) {}

  runHttpGet(url: string) {
    return this.http.get(`${url}`).pipe(
      map(data => {
        return data;
      })
    );
  }

  runHttpStore(url: string, obj, showAlert = true) {
    return this.http.post(`${url}`, obj).pipe(
      map(data => {
        if(showAlert) {this.snackService.showAlert('Se ha creado correctamente');}
        return data;
      })
    );
  }

  runHttpUpdate(url: string, obj, showAlert = true) {
    return this.http.put(`${url}`, obj).pipe(
      map(data => {
        if(showAlert) {this.snackService.showAlert('Se ha actualizado correctamente');}
        return data;
      })
    );
  }

  runHttpDestroySelected(url: string, ids = null, showAlert = true) {
    let params = new HttpParams();
    if(ids) {
      params = params.append('ids', ids);
    }
    return this.http.delete(`${url}`, { params: params }).pipe(
      map(data => {
        if(showAlert) {this.snackService.showAlert('Se ha eliminado correctamente');}
        return data;
      })
    );
  }

  runHttpDestroy(url: string, id, showAlert = true) {
    return this.http.delete(`${url}` + '/' + id).pipe(
      map(data => {
        if(showAlert) {this.snackService.showAlert('Se ha eliminado correctamente');}
        return data;
      })
    );
  }
}
