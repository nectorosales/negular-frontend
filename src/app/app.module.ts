import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { JitsiMeetComponent } from './shared/jitsi-meet/jitsi-meet.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { AuthHelperService } from './helpers/auth-helper.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { MeetingModule } from './views/meeting/meeting.module';
import { UserService } from './services/user/user.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {PathService} from './services/shared/path.service';
import {HomeModule} from './views/home/home.module';
import {AuthService} from './services/auth/auth.service';
import {AuthGuardService} from './guards/auth-guard.service';
import {RoleGuardService} from './guards/role-guard.service';
import {ErrorInterceptor} from './interceptors/error-interceptor.service';
import {AuthInterceptor} from './interceptors/auth-interceptor.service';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import {SnackBarService} from './services/shared/snackbar.service';
import {EventService} from './services/event/event.service';
import {AvailabilityService} from './services/availability/availability.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {DynamicScriptLoaderService} from './services/shared/dynamicScriptLoader.service';
import {LoaderService} from './services/shared/loader.service';
import {LoaderInterceptor} from './interceptors/loader-interceptor.service';
import {MeetingService} from './services/meeting/meeting.service';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import {SettingModule} from './views/setting/setting.module';
import {RoomService} from './services/room/room.service';
import { UsernameComponent } from './views/username/username.component';
import {QuestionService} from './services/question/question.service';
import {OptionService} from './services/option/option.service';
import {GlobalService} from './services/shared/global.service';
import {UserModule} from './views/user/user.module';
import {EventModule} from './views/event/event.module';
import {NotificationModule} from './views/notification/notification.module';
import {NotificationService} from './services/notification/notification.service';
import {RoomModule} from './views/room/room.module';
import {UserNotificationService} from './services/user-notification/user-notification.service';
import {DragDirective} from './directives/dragDrop.directive';
import {UserMeetingService} from './services/user-meeting/user-meeting.service';
import {PaymentModule} from './views/payment/payment.module';
import {DrawerService} from './services/shared/drawer.service';
import { UsernameEventFormComponent } from './views/username/username-event-form/username-event-form.component';
import {SlugifyPipe} from './pipes/slugify.pipe';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTooltipModule} from '@angular/material/tooltip';
import { UsernameEventFormPaymentComponent } from './views/username/username-event-form/username-event-form-payment/username-event-form-payment.component';
import {MatListModule} from '@angular/material/list';
registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, UsernameComponent, UsernameEventFormComponent, SlugifyPipe, UsernameEventFormPaymentComponent],
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
        MeetingModule,
        HttpClientModule,
        MatSnackBarModule,
        HomeModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatCheckboxModule,
        MatGridListModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        LayoutModule,
        SettingModule,
        UserModule,
        EventModule,
        NotificationModule,
        RoomModule,
        PaymentModule,
        MatStepperModule,
        MatTooltipModule,
        MatListModule
    ],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-ES'},
    AuthHelperService,
    UserService,
    PathService,
    SnackBarService,
    EventService,
    AuthService,
    AvailabilityService,
    AuthGuardService,
    RoleGuardService,
    LoaderService,
    MeetingService,
    RoomService,
    QuestionService,
    OptionService,
    GlobalService,
    NotificationService,
    UserNotificationService,
    DrawerService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
    DynamicScriptLoaderService,
    UserMeetingService,
    SlugifyPipe
  ],
  exports: [
    DragDirective
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
