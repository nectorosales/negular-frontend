import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {AuthService} from '../services/auth/auth.service';
import {SnackBarService} from '../services/shared/snackbar.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, public snackBarService: SnackBarService) { }

  processError(err) {
    if (err.status === 401) {
      this.authService.logout();
      this.snackBarService.showAlert('Tu sesión ha caducado');
    } else if (err.status === 403) {
      this.snackBarService.showAlert('No tienes permisos');
    } else if (err.status === 409) {
      this.snackBarService.showAlert('El usuario o email ya existe');
    } else {
      this.snackBarService.showAlert('No es posible realizar la petición');
    }
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {

      this.processError(err);
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
