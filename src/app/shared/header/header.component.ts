import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { SidenavService } from '../../services/shared/sidenav.service';
import {UserService} from '../../services/user/user.service';
import {DialogComponent} from '../dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {LoginComponent} from '../login/login.component';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {PathService} from '../../services/shared/path.service';
import {DrawerService} from '../../services/shared/drawer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  toggleActive = false;
  @Output() emitClick = new EventEmitter();

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    public sidenavService: SidenavService,
    private breakpointObserver: BreakpointObserver,
    public userService: UserService,
    private dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    public pathService: PathService,
    public drawerService: DrawerService
  ) {
  }

  toggleSidenav() {
    this.toggleActive = !this.toggleActive;
    this.sidenavService.toggle();
  }

  loginForm() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Acceso a Negular',
        component: LoginComponent,
        buttons_disabled: true
      },
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sidenavService.getNavigationSideMenu(result.data);
        this.userService.show(result.data.id).subscribe((user: any) => {
          setTimeout(() => {
            this.userService.setUser(user);
            this.router.navigate(['/admin/reuniones']);
            this.emitClick.emit(user);
          }, 1000);
        });
      }
    });
  }

  logout() {
    this.authService.logout();
  }

  toggleDrawer() {
    this.drawerService.toggle();
  }

  ngOnInit(): void {

  }
}
