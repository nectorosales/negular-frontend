import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import {MatDrawer, MatSidenav} from '@angular/material/sidenav';
import { SidenavService } from '../../services/shared/sidenav.service';
import {UserService} from '../../services/user/user.service';
import {AuthService} from '../../services/auth/auth.service';
import {PathService} from '../../services/shared/path.service';
import {LoaderService} from '../../services/shared/loader.service';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';
import {MeetingService} from '../../services/meeting/meeting.service';
import {DrawerService} from '../../services/shared/drawer.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('sidenav') public sidenav: MatSidenav;
  @ViewChild('drawer') public drawer: MatDrawer;
  isLoading = false;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  user: any;
  path: string = environment.apiEndpointPublic;

  constructor(
    private breakpointObserver: BreakpointObserver,
    public sidenavService: SidenavService,
    public drawerService: DrawerService,
    public userService: UserService,
    private authService: AuthService,
    public pathService: PathService,
    private loaderService: LoaderService,
    private cdRef : ChangeDetectorRef,
    private router: Router,
    public meetingService: MeetingService
  ) {}

  getLoader(){
    this.loaderService.isLoading.subscribe((v) => {
      this.isLoading = v;
      this.cdRef.detectChanges();
    });
  }

  ngOnInit() {
    const token = this.authService.decodeToken();
    if(token) {
      this.userService.show(token.id).subscribe((user: any) => {
        this.getMeetingRoom(user);
        this.sidenavService.getNavigationSideMenu(user);
      });
      this.pathService.getPath();
    }
    this.getLoader();
  }

  logout() {
    localStorage.removeItem('negular-token');
    this.userService.setUser(null);
    this.router.navigate(['']);
  }

  ngAfterViewInit() {
    this.sidenavService.setSidenav(this.sidenav);
    this.drawerService.setDrawer(this.drawer);
  }

  loadUser(user){
    this.userService.setUser(user);
    this.cdRef.detectChanges();
  }

  getMeetingRoom(user){
    this.loadUser(user);
    console.log(user)
    this.meetingService.indexByUser(user.id).subscribe( (meetings: any[]) => {
      this.meetingService.setMeetings(meetings);
    });
  }

  openMeetRoom(id) {
    if(id) {
      window.open(environment.jitsiMeet + id, '_blank');
    }
  }

  ngOnChanges(changes): void {
    if(this.userService.user) this.getMeetingRoom(this.userService.user);
  }

  openRouterLink(routerLink: string, drawer) {
    this.router.navigate([routerLink]);
    if (window.innerWidth <= 600) drawer.toggle();
  }
}
