import { Component, OnInit } from '@angular/core';
import {PathService} from '../../services/shared/path.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  date: Date;

  constructor(public pathService: PathService) {
  }

  ngOnInit(): void {
    this.date = new Date();
  }

}
