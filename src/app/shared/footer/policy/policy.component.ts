import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit {

  policy: any;
  policies: any[];

  constructor(private route: ActivatedRoute) {
    this.policies = [
      { key: 'privacidad',
        name: 'Política de privacidad',
        content: ' <h3>Fecha de entrada en vigor: 30 de octubre de 2020</h3>' +
          '    <p>Esta Política de uso aceptable (la «Política») rige todos los usos de los Servicios de Negular, Términos del servicio, y cualquier otro documento contractual para la prestación y el uso de la plataforma de Negular. Esta Política define las normas que Negular espera que sus Clientes y usuarios finales respeten al usar la Plataforma.<p>' +
          '    <p>La Plataforma de Negular está diseñada para promover la colaboración y el trabajo eficaz, la gestión de la empresa de una forma fácil y unificada en la Plataforma. Sus funcionalidades se centran en conectar a los usuarios mediante funciones de voz, vídeo y uso compartido de pantalla mediante un sistema seguro integrado en la plataforma; gestión de reuniones y reservas bajo disponibilidad personalizada; plataforma de pago sin comisiones integrada; gestión de usuarios (notificaciones, listado histórico de seguimiento y pagos de cada uno de ellos); gestión de notificaciones con plantillas personalizadas y espacio público para sus clientes.</p>' +
          '    <p>Los Clientes y Usuarios finales no pueden usar los Servicios para:<p>' +
          '    <ul>' +
          '    <li>Publicar o transmitir de ningún modo contenidos, incluidos vídeos en vivo, que infrinjan esta Política.</li>' +
          '    <li>Llevar a cabo actividades ilegales, facilitar cualquier actividad ilegal o promover la violencia.</li>' +
          '    <li>Llevar a cabo actividades que amenacen, exploten o perjudiquen de otro modo a niños.</li>' +
          '    <li>Participar en cualquier actividad que sea nociva, obscena o indecente (en particular, las que se considerarían como tales en el contexto de uso comercial). Esto comprende, por ejemplo, exhibiciones de desnudos, violencia, pornografía, material sexualmente explícito o actividades delictivas.</li>' +
          '    <li>Facilitar o apoyar el tráfico de personas.</li>' +
          '    <li>Participar de cualquier actividad que apoye o facilite el terrorismo o a organizaciones terroristas.</li>' +
          '    <li>Participar de cualquier actividad que sea fraudulenta, falsa o engañosa.</li>' +
          '    <li>Participar de cualquier actividad que sea difamatoria, acosadora, amenazadora o abusiva.</li>' +
          '    <li>Almacenar o transmitir cualquier dato o material que sea fraudulento, ilegal, difamatorio, amenazador, obsceno, indecente o inapropiado de otro modo.</li>' +
          '    <li>Enviar mensajes no autorizados, publicidad o correo basura, lo que comprende contenidos comerciales o promocionales u otros materiales masivos no deseados.</li>' +
          '    <li>Falsificar la identidad del Usuario o su afiliación con cualquier entidad u organización, o hacerse pasar por cualquier otra persona.</li>' +
          '    <li>Recabar, recoger o recopilar datos de usuarios sin su consentimiento.</li>' +
          '    <li>Vulnerar o infringir cualquier derecho de propiedad o propiedad intelectual de terceros, incluso derechos de autor.</li>' +
          '    <li>Vulnerar la privacidad o distribuir información personal o confidencial de terceros.</li>' +
          '    <li>Participar en cualquier actividad que perjudique o altere la Plataforma, o intente eludir las restricciones de acceso, uso o seguridad de esta. Esto comprende transmitir virus, malware u otros códigos maliciosos o destructivos, o utilizar herramientas para ocultar la ubicación de la dirección IP o eludir de otro modo las restricciones de uso debido a regulaciones o al cierre de cuentas.</li>' +
          '    </ul>' +
          '    <p>Si tiene conocimiento de algún incumplimiento de esta política, póngase en contacto con Negular en violación@negular.com. Negular investigará todos los informes y tomará medidas inmediatas y adecuadas si resulta procedente. Si un Cliente o Usuario final viola cualquiera de los términos de esta Política de uso aceptable, Negular podrá cerrar la cuenta del Cliente de inmediato, suspender o inhabilitar su acceso o tomar cualquier otra medida apropiada, incluidas acciones legales, si corresponde.</p>' +
          '    <h3>Uso razonable</h3>' +
          '    <p>Negular pone a su disposición esta plataforma con la finalidad de ayudarle a mejor la gestión de su empresa online de una manera sencilla y eficiente. Negular espera que los clientes usen la Plataforma de manera razonable, acorde a los fines comerciales. En consecuencia, Negular puede limitar, suspender o cancelar el acceso de un Usuario final si su uso no cumple con los estándares razonables, y puede supervisar el uso en función de todas la funcionalidades de la plataforma.</p>' +
          '    <p>Negular puede determinar que un uso es anormal, no razonable o no permisible en función de los estándares del sector y los patrones de uso, y puede tomar las medidas que sean necesarias, incluso la suspensión o la finalización del servicio. Negular podrá ponerse previamente en contacto con el Cliente y analizar el uso apropiado y los planes adecuados para un uso válido de la Plataforma.</p>'
      },
      {key: 'terminos-de-uso',
        name: 'Términos de uso',
        content: '<h4>LA PLATAFORMA BETA DE NEGULAR SE RIGE POR ESTOS TÉRMINOS DE USO. PUEDE CONTENER UNO O MAS SERVICIOS BETA SEGÚN LO DETERMINE NEGULAR. AL HACER USO DE LA PLATAFORMA BETA Y UTILIZAR SUS APLICACIONES, USTED ENTIENDE Y ACEPTA QUE NEGULAR TRATARÁ SU USO DE LA PLATAFORMA COMO ACEPTACIÓN DE LOS TERMINOS DE USO. SIEMPRE QUE CUMPLA CON ESTOS TÉRMINOS, NEGULAR LE OTORGA EL PRIVELIGO DE PODER UTILIZAR LA PLATAFORMA BETA.</h4>' +
          '    <p>Por la presente, igualmente reconoce que Negular no ha hecho ninguna declaración, promesa o garantía de que la plataforma estará a disposición de nadie en el futuro y por consiguiente de que no tiene ninguna obligación expresa o implícita con usted de anunciar o presentar la plataforma. Negular puede descontinuar la plataforma Beta en cualquier momento, a su entera discreción, con o sin previo aviso.</p>' +
          '    <h2>1. Funcionalidad</h2>' +
          '    <ul>' +
          '      <li><strong>1.1 Descripción.</strong> Usted tiene a su disposición las funcionalidades de la plataforma Beta de Negular. Se proporciona una descripción más detallada de las funcionalidades de la plataforma una vez que se registra en ella, se agrega una nueva funcionalidad o se actualiza a medida que avanza la Plataforma Beta.<</li>' +
          '      <li><strong>1.2 Estado.</strong> Al aceptar estos términos o utilizar la Plataforma Beta de Negular, usted comprende y reconoce que las funcionalidades se brindan como una versión “Beta” y que están disponibles “ Tal cómo están”. Dichas funcionalidades pueden contener errores y otros problemas que irán siendo subsanados. Negular recomienda que haga una copia de seguridad de todos los dato e información antes de utilizar la plataforma. USTED ASUME TODOS LOS RIESGOS Y TODOS LOS COSTOS ASOCIADOS CON EL USO QUE HAGA DE NUESTRAS FUNCIONALIDADES, INCLUYENDO SIN LIMITACIÓN, CUALQUIER CARGO DE ACCESO A INTERNET, GASTOS DE RESPALDO, COSTOS INCURRIDOS POR EL USO DE SU DISPOSITIVO Y PERIFÉRICOS, Y CUALQUIER DAÑO A CUALQUIER EQUIPO, SOFTWARE, INFORMACIÓN O DATO.</li>' +
          '      <li><strong>1.3 Retroalimentación.</strong> Las funcionalidades de la Plataforma Beta están a su disposición con fines de evaluación y comentarios sin ninguna compensación o reembolso por parte de Negular. Usted reconoce la importancia de la comunicación entre Negular y usted durante el uso de la plataforma y por tanto, acepta recibir comunicaciones y actualizaciones de Negular. En caso de que solicite la exclusión voluntaria de dichas comunicaciones, se cancelará su participación de la Plataforma Beta. Durante esta etapa beta, se le pedirá que proporcione comentarios sobre el uso de la plataforma beta. Usted reconoce que Negular es propietario de los comentarios proporcionados y, por la presente se otorga a Negular la licencia de usar y / o incorporar dichos comentarios en cualquier funcionalidad de la plataforma de Negular, en cualquier momento a la sola discreción de Negular. Negular no publicará comentarios de una manera que sea atribuible a usted sin su consentimiento.</li>' +
          '      <li><strong>1.4 Limitaciones de uso.</strong> Puede utilizar la Plataforma de Negular de conformidad con estos términos durante el período en el que la Plataforma Beta esté activa. Cuando esta no lo esté, ya no tendrá acceso a la plataforma Beta. Negular se reserva el derecho de modificar o imponer limitaciones en el uso de las funcionalidades en cualquier momento, con o sin previo aviso.</li>' +
          '    </ul>' +
          '    <h2>2. Participación en la Plataforma Beta.</h2>' +
          '    <ul>' +
          '    <li><strong>2.1. Elegibilidad.</strong> Puede participar en la Plataforma Beta a solicitud y aprobación de Negular, o por invitación de Negular. Sólo puede participar en la Plataforma Beta si es un Cliente en regla de Negular, sujeto a un “Acuerdo Subyacente” para el uso de la Plataforma y debe seguir siéndolo duarnte la duración del Programa Beta. El acuerdo subyacente y la política de privacidad de Negular controlan cualquier término que se trate específicamente en estos Términos. En el caso de que deje de ser cliente de Negular, la participación en la Plataforma Beta finalizará inmediatamente.</li>' +
          '    <li><strong>2.2. Control de los términos del acuerdo subayacente.</strong> A menos que estos Términos se modifiquen específicamente, las disposiciones del Acuerdo Subyacente continúan en plena vigencia con respecto a su uso , incluidas las disposiciones sobre la actividad de la cuenta, la seguridad de la contraseña, el contenido del ususario y las violaciones de seguridad.</li>' +
          '    <li><strong>2.3. Política de uso aceptable.</strong> Está sujeta a la Política de uso.</li>' +
          '    <li><strong>2.4. Ingeniería inversa.</strong> Excepto en la medida permitida por la ley, no puede modificar, distribuir, preparar trabajos derivados, realizar ingeniería inversa, desensamblar, descompilar o intentar descifrar cualquier código relacionado con las funcionalidades y / o cualquier otro aspecto de la tecnología de Negular.</li>' +
          '    </ul>' +
          '    <h2>3. Copyright.</h2>' +
          '    <p>Negular respeto los derechos de propiedad intelectual de otros. Si cree que las funcionalidades de la Plataforma se están utilizando de una manera que constituye una infracción de derechos de autor, notifíquenos según lo dispuesto en el Acuerdo Subyacente. Negular se reserva el derecho de eliminar o deshabilitar el contenido presuntamente infractor y de cancelar las cuentas de los usuarios infractores.</p>' +

          '    <h2>4. Propiedad intelectual.</h2>' +
          '    <p>Useted acepta que Negular posee todos los derechos legales, títulos e intereses en y para el Programa Beta, incluidos todos los derechos de propiedad intelectual, y excepto por la licencia proporcionada en este documento, no se otorgan otros derechos o permisos a ningunas de las funcionalidades. Nada de lo aquí contenido le otorga el derecho a utilizar ninguno de los nombres comerciales, marcas comerciales, marcas de servicio, logotipos, nombres de dominio y otras características distintivas de marca de Negular.</p>' +
          '    <h2>5. Modificación y Terminación de la Plataforma.</h2>' +
          '    <p>Negular se reserva el derecho de modificar o cancelar la Plataforma Beta, o su uso del Programa Beta, para limitar o denegar el acceso y / o la participación en la Plataforma Beta, en cualquier momento, en su único discreción, por cualquier motivo, con o sin previo aviso y sin responsabilidad ante usted. Puede interrumpir el uso de la Plataforma en cualquier momento.</p>' +

          '    <h2>6. \tRENUNCIA DE GARANTÍAS.</h2> <p>POR LA PRESENTE, USTED RECONOCE Y ACEPTA            QUE LAS FUNCIONALIDADES SON PROPORCIONADAS POR NEGULAR "TAL CUAL" Y SEGÚN ESTÁN DISPONIBLES, Y SU ACCESO Y / O USO DE LA PLATAFORMA BETA DE NEGULAR ES BAJO SU PROPIO RIESGO. EN LA MEDIDA PERMITIDA POR LA LEY APLICABLE, NEGULAR RENUNCIA EXPRESAMENTE A TODO Y USTED NO RECIBE GARANTÍAS NI CONDICIONES DE NINGÚN TIPO, YA SEAN EXPRESAS O IMPLÍCITAS, INCLUYENDO, PERO NO LIMITADO A, AQUELLOS DE COMERCIABILIDAD, SATISFACTORÍA Y CALIDAD, NO INFRACCIÓN. NEGULAR  NO GARANTIZA QUE CUALQUIERA DE LAS FUNCIONALIDADES CUMPLIRÁ CON SUS REQUISITOS Y / O QUE ESTAS SERÁN ININTERRUMPIDAS, OPORTUNAS O LIBRES DE ERRORES, NEGULAR  NO OFRECE NINGUNA GARANTÍA CON RESPECTO A LOS RESULTADOS QUE SE PUEDAN OBTENER DEL USO DE LA PLATAFORMA O DE LA EXACTITUD DE CUALQUIER OTRA INFORMACIÓN OBTENIDA A TRAVÉS DE SU USO. USTED ENTIENDE Y ACEPTA QUE CUALQUIER MATERIAL Y / O DATOS DESCARGADOS U OBTENIDOS DE OTRO MODO MEDIANTE EL USO DE CUALQUIERA DE LAS FUNCIONALIDADES DE LA PLATAFORMA SE HACE BAJO SU PROPIO RIESGO Y QUE USTED SERÁ EL ÚNICO RESPONSABLE POR CUALQUIER DAÑO A SU SISTEMA DE COMPUTADORA Y / O PÉRDIDA DE DATOS  RESULTADOS DE LA DESCARGA DE DICHO MATERIAL Y / O DATOS. NINGUNA INFORMACIÓN O CONSEJO, YA SEA ORAL O ESCRITO, OBTENIDO POR USTED DE NEGULAR  A TRAVÉS DE LAS FUNCIONALIDADES CREARÁ NINGUNA GARANTÍA QUE NO SE HAGA EXPRESAMENTE AQUÍ. ALGUNAS JURISDICCIONES NO PERMITEN LA EXCLUSIÓN DE CIERTAS GARANTÍAS Y CONDICIONES, POR LO QUE ALGUNAS DE LAS EXCLUSIONES ANTERIORES PUEDEN NO APLICARSE EN SU CASO.</p>' +
          '    <h2>7.   LIMITACIÓN DE RESPONSABILIDAD.</h2> <p>EN NINGÚN CASO NEGULAR SERÁ                 RESPONSABLE DE CUALQUIER PÉRDIDA INDIRECTA, ESPECIAL, CONSECUENTE Y / O INCIDENTAL, EJEMPLOS U OTROS DAÑOS RELACIONADOS CON ESTOS TÉRMINOS Y / O SEAN DIRECTOS O INDIRECTOS: (i) PÉRDIDA DE DATOS, (ii) PÉRDIDA DE INGRESOS, (iii) PÉRDIDA DE OPORTUNIDADES, (iv) PÉRDIDA DE BENEFICIOS, Y (v) COSTOS DE RECUPERACIÓN O CUALQUIER OTRO DAÑO, SIN EMBARGO, CAUSADO Y BASADO EN CUALQUIER TEORÍA DE RESPONSABILIDAD, YA SEA POR INCUMPLIMIENTO DE CONTRATO, AGRAVIO (INCLUYENDO NEGLIGENCIA), VIOLACIÓN DEL ESTATUTO, O DE OTRA MANERA, Y SI SE HA INFORMADO A NEGULAR O NO DE LA POSIBILIDAD DE DICHOS DAÑOS. EN LA MEDIDA EN QUE LO PERMITA LA LEY APLICABLE. ALGUNAS JURISDICCIONES NO PERMITEN LA LIMITACIÓN O EXCLUSIÓN DE RESPONSABILIDAD POR DAÑOS INCIDENTALES O CONSECUENTES, POR LO QUE ALGUNAS DE LAS LIMITACIONES ANTERIORES PUEDEN NO APLICARSE EN SU CASO.</p>' +
          '    <h2>8.    Indemnización .</h2> <p>Usted acepta eximir de responsabilidad e indemnizar a Negular de y contra cualquier reclamo de terceros que surja de o de alguna manera relacionada con (i) su incumplimiento de los Términos, (ii) su uso del Plataforma Beta y / o Funcionalidades, (iii) su violación de las leyes, reglas o regulaciones aplicables en relación con las Funcionalidades, o (iv) su contenido de usuario, incluyendo cualquier responsabilidad o gasto que surja de todas las reclamaciones, pérdidas, daños (reales y consecuentes), juicios, juicios, costas de litigios y honorarios de abogados, de todo tipo y naturaleza. En tal caso, Negular le proporcionará un aviso por escrito de dicho reclamo, demanda o acción.</p>' +
          '    <h2>9. \tInformación confidencial.</h2> <p>Usted reconoce y acepta que: (i) sujeto al subpárrafo (iv), el lanzamiento exitoso al mercado de las versiones comerciales de las funcionalidades requiere que mantenga todos los datos e información de Negular discutidos y / o disponibles a través de la Plataforma Beta o contenidos en ella, incluyendo, sin limitación, las funcionalidades (colectivamente la "Información Confidencial") estrictamente confidenciales; (ii) la divulgación prematura de cualquier información confidencial dañaría la ventaja competitiva de Negular y los derechos de propiedad intelectual; (iii) la Información Confidencial no se compartirá con nadie más que otros participantes autorizados de la misma Plataforma Beta; y (iv) solo la Información Confidencial que ha sido divulgada públicamente por Negular puede ser discutida o mostrada al público.</p>' +
          '    <h2>10. Cambios en los Términos .</h2> <p>Negular se reserva el derecho de realizar cambios en los Términos de vez en cuando y le notificará en tal caso. Usted comprende y acepta que si usa la Plataforma Beta después de la fecha en que los Términos han cambiado, Negular considerará su uso como una aceptación de los Términos actualizados.</p>' +
          '    <h2>11. Términos adicionales .</h2>' +
          '    <ul>' +
          '      <li>11.1. Cumplimiento de leyes .</li><p> Ambas partes acuerdan cumplir con todas las leyes, reglas y regulaciones locales, estatales, nacionales y extranjeras aplicables, incluidas, entre otras, todas las leyes y regulaciones de importación y exportación que rigen el uso, transmisión y / o comunicación de contenido, en relación con su desempeño, acceso y / o uso de la Plataforma.</p>' +
          '      <li>11.2. Uso internacional .</li><p> La Plataforma Beta está destinada a ser utilizada en España. Si elige acceder a la Plataforma Beta desde ubicaciones que no sea España, lo hace por su propia iniciativa y bajo su propio riesgo, y es responsable del cumplimiento de todas las leyes y regulaciones pertinentes. Negular no garantiza que la Plataforma sea apropiada y / o esté disponible para su uso en una ubicación en particular.</p>' +
          '    </ul>'},
      {
        key: 'cookies',
        name: 'Política de Cookies',
        content: '<h2>1. NORMATIVA APLICABLE</h2>' +
          '    <p>El apartado segundo del artículo 22 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de laInformación y de Comercio Electrónico (en adelante, LSSI-CE), establece lo siguiente:</p>' +
          '    <p>1. Los prestadores de servicios podrán utilizar dispositivos de almacenamiento y recuperación de datos en equipos terminales de los destinatarios, a condición de que los mismos hayan dado su consentimiento después de que se les haya facilitado información clara y completa sobre su utilización, en particular, sobre los fines del tartamiento de los datos, con arreglo a lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal.</p>' +
          '    <p>Cuando sea técnicamente posible y eficaz, el consentimiento del destinatario para aceptar el' +
          '    tratamiento de los datos podrá facilitarse mediante el uso de los parámetros adecuados del' +
          '      navegador o de otras aplicaciones, siempre que aquél deba proceder a su configuración durante su instalación o actualización mediante una acción expresa a tal efecto.</p>' +
          '    <p>Lo anterior no impedirá el posible almacenamiento o acceso de índole técnica al solo fin de efectuar la transmisión de una comunicación por una red de comunicaciones electrónicas o, en la medida que resulte estrictamente necesario, para la prestación de un servicio de la sociedad de la información expresamente solicitado por el destinatario.</p>' +
          '    <p>Según recoge la “Guía sobre el uso de las cookies”, publicada por la Agencia Española de Protección de Datas (en adelante, AEPD) en el año 2013, la LSSI-CE es aplicable a cualquier tipo de archivo o dispositivo que se descarga en el equipo terminal de un usuario con la finalidad de almacenar datos que podrán ser actualizados y recuperados por la entidad responsable de su instalación. La cookie es uno de esos dispositivos de uso generalizado por lo que, en adelante, denominaremos genéricamente estos dispositivos como cookies.</p>' +
          '    <p>Quedan exceptuadas del cumplimiento de las obligaciones establecidas en el artículo 22.2 de la LSSI-CE las cookies utilizadas para alguna de las siguientes finalidades:</p>' +
          '    <ul>' +
          '      <li>Permitir únicamente la comunicación entre el equipo del usuario y la red.</li>' +
          '      <li>Estrictamente prestar un servicio expresamente solicitado por el usuario.</li>' +
          '    </ul>' +
          '    <h2>2. TIPOS DE COOKIES SEGÚN SU FINALIDAD</h2>' +
          '    <p>Una cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.</p>' +
          '    <p>Tal como recoge la “Guía sobre el uso de las cookies” de la AEPD, según la finalidad para la que se traten los datos obtenidos a través de las cookies, podemos distinguir entre:</p>' +
          '    <p><strong>- Cookies técnicas:</strong> Son aquéllas que permiten al usuario la navegación a través de una página web,' +
          '    plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan' +
          '    como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a' +
          '    partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de' +
          '    compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar' +
          '    elementos de seguridad durante la navegación, almacenar contenidos para la difusión de videos o' +
          '      sonido o compartir contenidos a través de redes sociales.</p>' +
          '    <p><strong>- Cookies de personalización:</strong> Son aquéllas que permiten al usuario acceder al servicio con algunas' +
          '    características de carácter general predefinidas en función de una serie de criterios en el terminal' +
          '    del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al' +
          '      servicio, la configuración regional desde donde accede al servicio, etc.</p>' +
          '    <p><strong>-Cookies de análisis:</strong> Son aquéllas que permiten al responsable de las mismas, el seguimiento y' +
          '    análisis del comportamiento de los usuarios de los sitios web a los que están vinculadas. La' +
          '    información recogida mediante este tipo de cookies se utiliza en la medición de la actividad de los' +
          '    sitios web, aplicación o plataforma y para la elaboración de perfiles de navegación de los usuarios' +
          '    de dichos sitios, aplicaciones y plataformas, con el fin de introducir mejoras en función del análisis' +
          '      de los datos de uso que hacen los usuarios del servicio.</p>' +
          '    <p><strong>-Cookies publicitarias:</strong> Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los' +
          '    espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o' +
          '    plataforma desde la que presta el servicio solicitado en base a criterios como el contenido editado o' +
          '      la frecuencia en la que se muestran los anuncios.</p>' +
          '    <p><strong>-Cookies de publicidad comportamental:</strong> Son aquéllas que permiten la gestión, de la forma más' +
          '    eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página' +
          '    web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan' +
          '    información del comportamiento de los usuarios obtenida a través de la observación continuada de' +
          '    sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en' +
          '      función del mismo.</p>' +
          '    <h2>3. PRINCIPIO DE INFORMACIÓN</h2>' +
          '    <p>En cumplimiento de lo establecido en el artículo 5 de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (en adelante, LOPD), le informamos de modo expreso, preciso e inequívoco que la información que se obtenga a través de las cookies que se instalen en su ordenador será utilizada con la finalidad de recoger datos de navegación en la web con el finde mejor la calidad de nuestros servicios.</p>' +
          '    <h2>4. PRINCIPIO DEL CONSENTIMIENTO</h2>' +
          '    <p>El consentimiento para la instalación de las cookies se entenderá prestado a través de la marcación de la casilla relativa a la aceptación de la “Política de cookies” dispuesta al efecto en nuestra página web.</p>' +
          '    <p>En los casos en que el usuario no manifieste expresamente si acepta o no la instalación de las cookies, pero continúe utilizando la página web o la aplicación se entenderá que éste ha dado su consentimiento,informándole expresamente nuestra entidad de la posibilidad de bloquear o eliminar las cookies instaladas' +
          '      en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador.</p>' +
          '    <h2>5. CARÁCTER FACULTATIVO DE LA INSTALACIÓN DE COOKIES</h2>' +
          '    <p>Si bien la aceptación de la instalación de las cookies en su ordenador es facultativa para usted, la negativa a su instalación puede suponer que la funcionalidad de la página web quede limitada o no sea posible, lo cual imposibilitaría la prestación de servicios por parte de nuestra entidad a través de la mismo.</p>' +
          '    <h2>6. DESACTIVACIÓN DE COOKIES</h2>' +
          '    <p>El usuario en todo momento podrá cambiar la configuración de las cookies, bloquearlas o desactivarlas. Para ello le facilitamos el modo en los principales navegadores.</p>' +
          '    <ul>' +
          '      <li>CHROME: https://support.google.com/chrome/answer/95647?hl=es</li>' +
          '      <li>EXPLORER: http://windows.microsoft.com/es-es/windows-vista/Block-or-allow-cookies</li>' +
          '      <li>FIREFOX: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</li>' +
          '      <li>SAFARI: http://www.apple.com/es/privacy/use-of-cookies/</li>' +
          '      <li>los terceros.</li>' +
          '    </ul>' +
          '    <h2>7. PRINCIPIO DE SEGURIDAD DE LOS DATOS</h2>' +
          '    <p>Negular se compromete al cumplimiento de su obligación de secreto respecto de los datos' +
          '    de carácter personal y de su deber de guardarlos y adoptará todas las medidas de índole técnica y' +
          '      organizativa necesarias que garanticen la seguridad de los datos de carácter personal y eviten su alteración, pérdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que estén expuestos, ya provengan de la acción humana o del medio físico o natural, desarrolladas en el Título VIII del Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal.</p>' +
          '    <h2>8. EJERCICIO DE DERECHOS</h2>' +
          '    <p>En cumplimiento de lo establecido en la LOPD y el Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la misma, el destinatario del servicio puede ejercitar, en cualquier momento, sus derechos de acceso, rectificación, cancelación y oposición ante el responsable del fichero o del tratamiento, adjuntando fotocopia de su DNI.</p>'
      }
    ];
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.policy = this.policies.filter(policy => {
        return policy.key === param.key;
      })[0];
    });
  }

}
