import {Component, Input, OnChanges} from '@angular/core';
import '../../../vendor/jitsi/external-api.js';
import {environment} from '../../../environments/environment';

declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-jitsi-meet',
  templateUrl: './jitsi-meet.component.html',
  styleUrls: ['./jitsi-meet.component.css']
})
export class JitsiMeetComponent implements OnChanges {

  domain: string = environment.apiJitsiMeet;
  options: any;
  api: any;
  @Input() room;

  constructor() {}

  ngOnChanges(): void {
    const token = localStorage.getItem('negular-token');
    this.options = {
      roomName: this.room,
      width: '100%',
      height: '100%',
      jwt: token,
      parentNode: document.querySelector('#meet'),
    };

    this.api = new JitsiMeetExternalAPI(this.domain, this.options);
  }
}
