import {
  AfterViewInit,
  ChangeDetectorRef,
  Compiler,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input, OnChanges, OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

@Component({
  selector: "app-component",
  templateUrl: "./component.component.html",
})
export class ComponentComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild("target", { static: false, read: ViewContainerRef }) target;
  @Input() component;
  @Input() data;
  cmpRef: ComponentRef<any>;
  private isViewInitialized = false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private compiler: Compiler,
    private cdRef: ChangeDetectorRef
  ) {}

  updateComponent() {
    if (!this.isViewInitialized) {
      return;
    }
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }

    const factory = this.componentFactoryResolver.resolveComponentFactory(
      this.component
    );
    this.cmpRef = this.target.createComponent(factory);
    this.cmpRef.instance.someProp = this.data;
    this.cdRef.detectChanges();
  }

  ngOnChanges() {
    this.updateComponent();
  }

  ngAfterViewInit() {
    this.isViewInitialized = true;
    this.updateComponent();
  }

  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }

  ngOnInit(): void {}
}
