import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../../helpers/must-match';
import {environment} from '../../../../environments/environment';
import {FileHandle} from '../../../directives/dragDrop.directive';
import {NgxImageCompressService} from 'ngx-image-compress';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {

  @Output() emitForm = new EventEmitter();
  registerForm: FormGroup;
  hide = true;
  web: string;
  files: FileHandle[] = [];
  loadFile;

  constructor(
    private fb: FormBuilder,
    public imageCompress: NgxImageCompressService
  ) {
    this.createForm();
    this.web = environment.domain;
  }

  filesDropped(files: FileHandle[]): void {
    this.loadFile = files[0].url;
    const reader = new FileReader();
    reader.readAsDataURL(files[0].file);
    reader.onload = () => {
      this.compressImage(reader.result, null);
    };
  }

  compressFile() {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.compressImage(image, orientation);
    });
  }

  compressImage(image, orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        this.loadFile = result; //base64
        const file = this.convertDataURIToFile(result);
        this.registerForm.controls['img'].setValue(file);
        console.warn('Size in KB is now:', this.imageCompress.byteCount(result)/1024);
      }
    );
  }

  convertDataURIToFile(dataurl) {
    let arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], 'fileName.' + mime.split('/')[1], {type:mime});
  }


  createForm() {
    this.registerForm = this.fb.group({
      img: ['', [Validators.required]],
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confirm_password: ['', [Validators.required]]
    }, {validator: MustMatch('password', 'confirm_password') });
  }

  onSubmit() {
    if(!(this.registerForm.invalid || this.registerForm.pristine)) {
      this.emitForm.emit(this.registerForm);
    }
  }

}
