import { Component } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {MatDialogRef} from '@angular/material/dialog';
import {DialogComponent} from '../dialog/dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private authService: AuthService, public dialogRef: MatDialogRef<DialogComponent>, private router: Router){}

  signIn(event) {
    this.authService.signIn(event.value).subscribe( (res: any) => {
      if (res.token) {
        localStorage.setItem('negular-token', res.token);
        this.dialogRef.close({
          data: this.authService.decodeToken()
        });
        return true;
      }
    });
  }

  signUp(event) {
    const obj = {
      img: event.value.img,
      name: event.value.name,
      lastName: event.value.lastName,
      phone: event.value.phone,
      email: event.value.email,
      username: event.value.username,
      password: event.value.password,
    }
    this.authService.signUp(obj).subscribe( (res: any) => {
      if (res.token) {
        localStorage.setItem('negular-token', res.token);
        this.dialogRef.close({
          data: this.authService.decodeToken()
        });
        return true;
      }
    });
  }
}
