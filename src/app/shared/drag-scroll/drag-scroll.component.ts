import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-drag-scroll',
  templateUrl: './drag-scroll.component.html',
  styleUrls: ['./drag-scroll.component.css']
})
export class DragScrollComponent implements OnChanges {

  @Input() data: any[];
  @Input() isEvent: boolean;
  @Output() emitClick = new EventEmitter();

  constructor() { }

  clickItem(item){
    this.emitClick.emit(item);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

}
