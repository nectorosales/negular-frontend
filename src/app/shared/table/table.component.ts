import {
  Component,
  Input,
  EventEmitter,
  OnChanges,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {DialogComponent} from '../dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../services/shared/snackbar.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnChanges {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<any>;

  @Output()
  emitClick = new EventEmitter();

  @Output()
  emitBtnClick = new EventEmitter();

  @Output()
  emitSelection = new EventEmitter();

  @Input() data: any;
  doesDataExist: boolean = false;
  dataSource = new MatTableDataSource([]);

  selection = new SelectionModel<any>(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
    } row ${row.position + 1}`;
    this.emitSelection.emit(this.selection.selected);
  }

  constructor(private dialog: MatDialog, private snackBarService: SnackBarService) {}

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openLink(obj, event) {
    obj.event = event;
    this.emitClick.emit(obj);
    this.selection.clear();
  }

  ngOnChanges() {
    if (this.data) {
      this.useData();
    }
  }

  useData() {
    this.doesDataExist = true;
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.data.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
    }, 500);
  }

  public destroy() {
    if(this.selection.selected.length === 0) {
      this.snackBarService.showAlert("Debes seleccionar algún elemento del listado");
      return;
    }

    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: 'Atención', content: this.data.contentDialog ? this.data.contentDialog : '¿Estas seguro de eliminarlos?' },
      disableClose: true,
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.emitSelection.emit(this.selection.selected);
        this.selection.clear();
      }
    });
  }

  eventClicked($event: MouseEvent) {
    this.emitBtnClick.emit($event);
  }
}
