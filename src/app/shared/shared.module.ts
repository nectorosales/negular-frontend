import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SidenavComponent} from './sidenav/sidenav.component';
import {RouterModule} from '@angular/router';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {HeaderComponent} from './header/header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {SidenavService} from '../services/shared/sidenav.service';
import {MatButtonModule} from '@angular/material/button';
import { FooterComponent } from './footer/footer.component';
import {DialogComponent} from './dialog/dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ComponentComponent} from './component/component.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {TableComponent} from './table/table.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {PathComponent} from './path/path.component';
import {MatMenuModule} from '@angular/material/menu';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import { SigninFormComponent } from './login/signin-form/signin-form.component';
import { SignupFormComponent } from './login/signup-form/signup-form.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SpinnerComponent } from './spinner/spinner.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {JitsiMeetComponent} from './jitsi-meet/jitsi-meet.component';
import {DragDirective} from '../directives/dragDrop.directive';
import { PolicyComponent } from './footer/policy/policy.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { DragScrollComponent } from './drag-scroll/drag-scroll.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [
    SidenavComponent,
    HeaderComponent,
    FooterComponent,
    DialogComponent,
    ComponentComponent,
    TableComponent,
    PathComponent,
    LoginComponent,
    SigninFormComponent,
    SignupFormComponent,
    SpinnerComponent,
    JitsiMeetComponent,
    DragDirective,
    PolicyComponent,
    DragScrollComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    DragScrollModule,
    MatSlideToggleModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    DialogComponent,
    ComponentComponent,
    TableComponent,
    LoginComponent,
    JitsiMeetComponent,
    DragDirective,
    SignupFormComponent,
    DragScrollComponent
  ],
  providers: [SidenavService]
})
export class SharedModule { }
