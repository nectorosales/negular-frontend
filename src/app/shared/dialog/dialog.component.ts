import { Component, OnInit, Inject } from "@angular/core";
import {DomSanitizer} from '@angular/platform-browser';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: "app-dialog",
  templateUrl: "./dialog.component.html",
  styleUrls: ["./dialog.component.css"]
})
export class DialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.data.content = this.data.content ? this.sanitizer.bypassSecurityTrustHtml(this.data.content) : null;
  }

  do() {
    this.dialogRef.close({ data: this.data });
    return true;
  }
}
