import {Component} from '@angular/core';
import {PathService} from "../../services/shared/path.service";
import {GlobalService} from '../../services/shared/global.service';

@Component({
  selector: 'app-path',
  templateUrl: './path.component.html',
  styleUrls: ['./path.component.css']
})
export class PathComponent {

  constructor(public pathService: PathService, private globalService: GlobalService) { }

  backClicked() {
    this.globalService.backClicked();
  }

}
