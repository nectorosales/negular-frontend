import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class UserMeetingService {

  uri = environment.apiEndpoint + "user-meetings";

  constructor(private authHelperService: AuthHelperService) {}

  getAllByUser(userId, clientId) {
    return this.authHelperService.runHttpGet(this.uri + '/' + userId + '/user/' + clientId);
  }

  store(obj) {
    return this.authHelperService.runHttpStore(this.uri, obj, false);
  }

  update(obj) {
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, obj, false);
  }

  destroySelected(ids) {
    return this.authHelperService.runHttpDestroySelected(this.uri, ids, false);
  }

  clients(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id + '/clients' );
  }

  payments(id: any) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id + '/payments' );
  }
}
