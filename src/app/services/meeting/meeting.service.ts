import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  uri = environment.apiEndpoint + "meetings";
  meetings: any[] = [];

  constructor(private authHelperService: AuthHelperService) {}

  setMeetings(meetings){
    if(meetings) {
      this.meetings = meetings.map( (meeting => {
        meeting.image = environment.apiEndpointPublic + meeting.event.img;
        return meeting;
      }));
    } else {
      this.meetings = [];
    }
    console.log(this.meetings);
  }

  index(eventId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/event/' + eventId);
  }

  indexByUser(userId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/user/' + userId);
  }

  show(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  store(obj) {
    return this.authHelperService.runHttpStore(this.uri, obj);
  }

  update(obj) {
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, obj);
  }

  destroy(id) {
    return this.authHelperService.runHttpDestroy(this.uri, id);
  }

  destroySelected(ids: any[]) {
    return this.authHelperService.runHttpDestroySelected(this.uri, ids, false);
  }

  indexByRoom(roomId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/room/' + roomId);
  }

  getMeetingRoomByUser(userId: any) {
    return this.authHelperService.runHttpGet(this.uri + '/user/' + userId + '/meetings');
  }
}
