import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { Router} from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import {UserService} from '../user/user.service';
import {SnackBarService} from '../shared/snackbar.service';
import {SidenavService} from '../shared/sidenav.service';
import {GlobalService} from '../shared/global.service';
import {MeetingService} from '../meeting/meeting.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  uri = environment.apiEndpoint + 'auth';

  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private document: Document,
    public userService: UserService,
    private router: Router,
    public snackBarService: SnackBarService,
    private globalService: GlobalService,
    private meetingService: MeetingService
  ) {}


  signIn(obj) {
    return this.http.post(`${this.uri}/signin`, obj).pipe(
      map((token: any) => {
        return token;
      }),
      catchError(err => {
        this.snackBarService.showAlert('Los datos introducidos son incorrectos');
        return err;
      })
    );
  }

  logout() {
    localStorage.removeItem('negular-token');
    this.userService.setUser(null);
    this.meetingService.setMeetings(null);
    this.router.navigate(['']);
  }

  decodeToken() {
    const token = localStorage.getItem('negular-token');
    if (token) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(token);
      return decodedToken;
    }
    return null;
  }

  tokenValid() {
    const token = this.decodeToken();
    if(token){
      if (Date.now() >= token.exp * 1000) {
        this.logout();
        this.snackBarService.showAlert('La sesión ha cadudado, por favor vuelva a iniciar sesión');
        return false;
      }
      /*this.userService.show(token.id).subscribe( user => {
        this.userService.setUser(user);
      });*/
      return true;
    } else {
      this.router.navigate(['/404']);
      return false;
    }
  }

  signUp(obj) {
    let formData = this.globalService.createFormData(obj);
    return this.http.post(`${this.uri}/signup`, formData).pipe(
      map((data: any) => {
        return data;
      }),
      catchError(err => {
        return err;
      })
    );
  }
}
