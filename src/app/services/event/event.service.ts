import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';
import {GlobalService} from '../shared/global.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  uri = environment.apiEndpoint + "events";

  constructor(private authHelperService: AuthHelperService, private globalService: GlobalService) {}

  index() {
    return this.authHelperService.runHttpGet(this.uri);
  }

  indexByUser(userId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/user/' + userId);
  }

  show(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  store(obj) {
    let formData = this.globalService.createFormData(obj);
    return this.authHelperService.runHttpStore(this.uri, formData);
  }

  update(obj) {
    let formData = this.globalService.createFormData(obj);
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, formData);
  }

  destroySelected(ids) {
    return this.authHelperService.runHttpDestroySelected(this.uri, ids);
  }

  getPublicEvent(username, slug){
    return this.authHelperService.runHttpGet(this.uri + '/username/' + username + '/' + slug);
  }
}
