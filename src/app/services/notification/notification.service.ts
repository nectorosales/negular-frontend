import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  uri = environment.apiEndpoint + "notifications";

  constructor(private authHelperService: AuthHelperService) {}

  indexByUser(userId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/user/' + userId);
  }

  show(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  store(obj) {
    return this.authHelperService.runHttpStore(this.uri, obj);
  }

  update(obj) {
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, obj);
  }

  destroySelected(ids) {
    return this.authHelperService.runHttpDestroySelected(this.uri, ids);
  }

  sendEmail(obj) {
    return this.authHelperService.runHttpStore(this.uri + '/sendEmail', obj);
  }
}
