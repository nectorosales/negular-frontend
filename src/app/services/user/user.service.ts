import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';
import {Router} from '@angular/router';
import {GlobalService} from '../shared/global.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  uri = environment.apiEndpoint + "users";

  public user: any;

  constructor(private authHelperService: AuthHelperService, private globalService: GlobalService) {

  }

  index() {
    return this.authHelperService.runHttpGet(this.uri);
  }

  show(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  getUsername(username){
    return this.authHelperService.runHttpGet(this.uri + '/username/' + username);
  }

  storeOrUpdate(obj) {
    return this.authHelperService.runHttpStore(this.uri, obj, false);
  }

  update(obj) {
    let formData = this.globalService.createFormData(obj);
    console.log(formData)
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, formData);
  }

  destroy(obj) {
    return this.authHelperService.runHttpDestroySelected(this.uri + '/' + obj.id);
  }

  setUser(user: any) {
    this.user = user;
  }


}
