import { Injectable } from '@angular/core';
import {Location} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private location: Location){

  }

  /*Funcionalidad para ordenar un Array*/

  sortBy(array, prop: string, order = 'asc') {
    return array.sort((a, b) => order === 'asc' && a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }

  /*Funcionalidad para comparar Arrays*/

  comparer(otherArray, prop = 'id'){
    return function(current){
      return otherArray.filter(function(other){
        return other[prop] && other[prop] == current[prop]
      }).length == 0;
    }
  }

  /*Funcionalidad para contruir un FormData a partir de un obj*/

  createFormData(obj){
    const formData = new FormData();
    Object.keys(obj).map(key => {
      formData.append(key, obj[key]);
    });
    console.log(formData)
    return formData;
  }

  /*Funcionalidad para retroceder a la última ruta seleccionada*/

  backClicked() {
    this.location.back();
  }

}
