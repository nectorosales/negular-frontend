import { Injectable } from '@angular/core';
import {MatDrawer} from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root'
})
export class DrawerService {
  drawer: MatDrawer;

  constructor() {}

  public setDrawer(drawer: MatDrawer) {
    this.drawer = drawer;
  }

  public toggle(): void {
    this.drawer.toggle();
  }

  public opened() {
    return this.drawer.opened;
  }
}
