import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PathService {
  public path: any[] = [];
  public isHome: boolean = false;

  constructor(
    private router: Router,
  ) {
    //path dynamic
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.getPath();
      }
    });
  }

  getPath() {
    this.path = [];
    const urls = this.router.url.split('/');
    let routerLink = '';
    urls.forEach(url => {
      if (url && url !== '/' && url !== 'inicio') {
        this.isHome = false;
        routerLink += '/' + url;
        this.path.push({label: '/ ' + url, routerLink: routerLink});
      } else {
        this.isHome = true;
      }
    });
  }
}
