import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import {Roles} from '../../enums/Roles';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private sidenav: MatSidenav;

  defaultMenu: any = [];

  constructor(public UserService: UserService) {}

  public getNavigationSideMenu(user) {
    this.defaultMenu = [];
    setTimeout(() => {
      /*if(user.roles?.includes(Roles.CLIENT) || user.roles?.filter(r => r.name === Roles.CLIENT)[0]){
        this.defaultMenu.push(
          {
            label: 'Meetings',
            icon: 'cast_connected',
            routerLink: '/admin/meetings',
            color: 'white',
            background: '#73e0bd'
          }
        );
      }*/
      if(user.roles?.includes(Roles.USER) || user.roles?.filter(r => r.name === Roles.USER)[0]) {
        this.defaultMenu.push(
          {
            label: 'Reuniones',
            icon: 'calendar.png',
            routerLink: '/admin/reuniones'
          },
          {
            label: 'Eventos',
            icon: 'events.png',
            routerLink: '/admin/eventos'
          },
          /*{
            label: 'Salas',
            icon: 'meeting_room',
            routerLink: '/admin/salas'
          },*/
          {
            label: 'Notificaciones',
            icon: 'mails.png',
            routerLink: '/admin/notificaciones'
          },
          {
            label: 'Usuarios',
            icon: 'users.png',
            routerLink: '/admin/usuarios'
          },
          {
            label: 'Pagos',
            icon: 'payments.png',
            routerLink: '/admin/pagos'
          },
          {
            label: 'Ajustes',
            icon: 'setting.png',
            routerLink: '/admin/ajustes'
          },
          {
            label: 'Presentación',
            icon: 'presentation.png',
            routerLink: '/' + this.UserService.user?.username
          }
        );

      } else {
        this.defaultMenu.push(
          {
            label: 'Registrar',
            icon: 'person',
            routerLink: '/admin/reuniones'
          }
        );
      }
    }, 1500);
  }

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  public toggle(): void {
    this.sidenav.toggle();
  }
}
