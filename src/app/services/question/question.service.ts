import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  uri = environment.apiEndpoint + "questions";

  constructor(private authHelperService: AuthHelperService) {}

  index(eventId: string) {
    return this.authHelperService.runHttpGet(this.uri + '/event/' + eventId);
  }

  show(id) {
    return this.authHelperService.runHttpGet(this.uri + '/' + id);
  }

  store(obj) {
    return this.authHelperService.runHttpStore(this.uri, obj, false);
  }

  update(obj) {
    return this.authHelperService.runHttpUpdate(this.uri + '/' + obj.id, obj, false);
  }

  destroySelected(ids) {
    return this.authHelperService.runHttpDestroySelected(this.uri, ids, false);
  }
}
