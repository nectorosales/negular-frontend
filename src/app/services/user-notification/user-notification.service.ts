import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthHelperService} from '../../helpers/auth-helper.service';

@Injectable({
  providedIn: 'root'
})
export class UserNotificationService {

  uri = environment.apiEndpoint + "user-notifications";

  constructor(private authHelperService: AuthHelperService) {

  }

  getAllByUser(userId, clientId) {
    return this.authHelperService.runHttpGet(this.uri + '/' + userId + '/user/' + clientId);
  }

}
