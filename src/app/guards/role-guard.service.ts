import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot, CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from "rxjs";
import {AuthService} from '../services/auth/auth.service';

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(
    public router: Router,
    public authService: AuthService
  ) {}

  canActivate(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const expectedRoles = childRoute.data.roles;
    let userRoles = this.authService.decodeToken()?.roles;
    for (let i = 0; i < userRoles.length; i++) {
      const userRole = userRoles[i];
      if (expectedRoles.includes(userRole)) {
        return true;
      }
    }
    this.router.navigate(['/404']);
    return false;
  }
}
