import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { ProfileComponent } from './views/setting/profile/profile.component';
import { AuthGuardService } from './guards/auth-guard.service';
import {UsernameComponent} from './views/username/username.component';
import {RoleGuardService} from './guards/role-guard.service';
import {Roles} from './enums/Roles';
import {PolicyComponent} from './shared/footer/policy/policy.component';
import {UsernameEventFormComponent} from './views/username/username-event-form/username-event-form.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./views/home/home.module').then(m => m.HomeModule)
  },
  { path: '404', component: PageNotFoundComponent },
  {
    path: 'admin',
    canActivate: [RoleGuardService],
    data: { roles: [Roles.USER, Roles.CLIENT] },
    children: [
      {
        path: '',
        redirectTo: 'reuniones',
        pathMatch: 'full'
      },
      {
        path: 'reuniones',
        loadChildren: () =>
          import('./views/meeting/meeting.module').then(m => m.MeetingModule)
      },
      {
        path: 'eventos',
        loadChildren: () =>
          import('./views/event/event.module').then(m => m.EventModule)
      },
      {
        path: 'notificaciones',
        loadChildren: () =>
          import('./views/notification/notification.module').then(m => m.NotificationModule)
      },
      {
        path: 'pagos',
        loadChildren: () =>
          import('./views/payment/payment.module').then(m => m.PaymentModule)
      },
      {
        path: 'usuarios',
        loadChildren: () =>
          import('./views/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'ajustes',
        component: ProfileComponent,
        canActivate: [AuthGuardService]
      },
    ]
  },
  {
    path: 'politica/:key',
    component: PolicyComponent,
  },
  {
    path: ':username',
    component: UsernameComponent,
  },
  {
    path: ':username/:event',
    component: UsernameEventFormComponent,
  },
  { path: '**', redirectTo: '404', pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      anchorScrolling: 'enabled',
      scrollOffset: [0, 0],
      scrollPositionRestoration: 'top'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
