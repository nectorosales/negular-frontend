export enum StateMeeting {
  PENDING = 'pending',
  FINISHED = 'finished',
  CANCELLED = 'cancelled',
}
