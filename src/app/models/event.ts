export class Event {
  id: string;
  img: string;
  name: string;
  typeEvent: string;
  description: string;
  colour: string;
  duration: number;
  price: string;
  active: boolean;
}
