import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {UserListComponent} from './user-list/user-list.component';
import {UserFormComponent} from './user-form/user-form.component';

const routes: Routes = [
  {
    path: '',
    component: UserListComponent,
    canActivate: [AuthGuardService],
    canActivateChild: [RoleGuardService],
    data: { roles: [Roles.USER] },
  },
  {
    path: ':id',
    component: UserFormComponent,
    canActivate: [AuthGuardService],
    canActivateChild: [RoleGuardService],
    data: { roles: [Roles.USER] },
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
