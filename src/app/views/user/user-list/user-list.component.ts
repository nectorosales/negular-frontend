import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {TableComponent} from '../../../shared/table/table.component';
import {UserMeetingService} from '../../../services/user-meeting/user-meeting.service';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  data = { data: [], search: false, columns: [], displayColumns: [], load: false };
  @ViewChild('tableComponent') tableComponent;

  constructor(private router: Router, private userMeetingService: UserMeetingService, public userService: UserService) {
    this.tableComponent = TableComponent;
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    setTimeout(() => {
      if(this.userService.user) {
        this.userMeetingService.clients(this.userService.user.id).subscribe((data: any[]) => {
          this.data = {
            data: data.map( (u: any) => {
              u.countMeeting = u.userMeetings?.length;
              return u;
            }),
            search: true,
            columns: [
              { name: 'email', label: 'Email' },
              { name: 'countMeeting', label: 'Reuniones' },
            ],
            load: true,
            displayColumns: [
              'select',
              'email',
              'countMeeting',
            ],
          };

        });
      }
    }, 500);
  }

  openForm(user) {
    this.router.navigate(['/admin/usuarios/' + user.id])
  }

  sendEmail(selected) {

  }
}
