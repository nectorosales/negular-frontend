import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {UserFormComponent} from './user-form/user-form.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserComponent} from './user.component';
import {SharedModule} from '../../shared/shared.module';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import { UserFormNotificationComponent } from './user-form/user-form-notification/user-form-notification.component';
import { UserFormMeetingComponent } from './user-form/user-form-meeting/user-form-meeting.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {NotificationModule} from '../notification/notification.module';

@NgModule({
  declarations: [UserComponent, UserFormComponent, UserListComponent, UserFormNotificationComponent, UserFormMeetingComponent],
  exports: [
    UserFormComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    MatStepperModule,
    MatSidenavModule,
    NotificationModule
  ]
})
export class UserModule {}
