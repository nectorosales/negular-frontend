import {Component, Input, OnChanges, ViewChild} from '@angular/core';
import {UserService} from '../../../../services/user/user.service';
import {UserMeetingService} from '../../../../services/user-meeting/user-meeting.service';
import * as moment from 'moment';
import 'moment-timezone';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-form-meeting',
  templateUrl: './user-form-meeting.component.html',
  styleUrls: ['./user-form-meeting.component.css']
})
export class UserFormMeetingComponent implements OnChanges {

  @Input() user;
  @Input() meeting;
  public data: {
    search: boolean;
    displayColumns: string[];
    data: any[];
    load: boolean;
    columns: (
      | { name: string; label: string; format?: string }
      | { name: string; label: string; type: string }
      )[];
  };
  @ViewChild('tableComponent') tableComponent;

  constructor(private router: Router, private userMeetingService: UserMeetingService, public userService: UserService) { }

  getUserMeetings(){

    this.userMeetingService.getAllByUser(this.userService.user.id, this.user.id).subscribe( (userMeetings: any[]) => {
      userMeetings = userMeetings.filter( (userMeeting: any) => {
        userMeeting.event = userMeeting.meeting.event.name;
        userMeeting.price = userMeeting.meeting.event.price + ' €';
        userMeeting.start = moment(userMeeting.meeting.start);
        return userMeeting;
      });

      this.data = {
        data: userMeetings,
        search: true,
        columns: [
          { name: 'event', label: 'Evento' },
          { name: 'price', label: 'Precio' },
          { name: 'state_payment', label: 'Pago' },
          { name: 'start', label: 'Comienzo', type: 'date', format: 'dd/MM/yy --- HH:mm a' },
        ],
        load: true,
        displayColumns: ['event', 'price', 'state_payment', 'start'],
      };
    });
  }

  ngOnChanges(){
    if(this.userService.user && (this.user || this.meeting)){
      this.getUserMeetings();
    }
  }

  routerLink(event) {
    this.router.navigate(['/admin/reuniones/' + event.meeting.id])
  }
}
