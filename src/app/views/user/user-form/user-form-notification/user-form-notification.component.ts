import {Component, Input, OnChanges, ViewChild} from '@angular/core';
import {UserNotificationService} from '../../../../services/user-notification/user-notification.service';
import {MatDrawer} from '@angular/material/sidenav';
import {UserService} from '../../../../services/user/user.service';

@Component({
  selector: 'app-user-form-notification',
  templateUrl: './user-form-notification.component.html',
  styleUrls: ['./user-form-notification.component.css']
})
export class UserFormNotificationComponent implements OnChanges {

  notification: any;
  @Input() user;
  @Input() meeting;
  public data: {
    search: boolean;
    displayColumns: string[];
    data: any[];
    load: boolean;
    columns: (
      | { name: string; label: string; format?: string }
      | { name: string; label: string; type: string }
      )[];
  };
  @ViewChild('tableComponent') tableComponent;

  constructor(private userNotificationService: UserNotificationService, public userService: UserService) { }

  getUserNotifications(){
    this.userNotificationService.getAllByUser(this.userService.user.id, this.user.id).subscribe( (userNotifications: any[]) => {

      console.log(userNotifications)
      userNotifications = userNotifications.filter( (userNotification: any) => {
        userNotification.event = userNotification.meeting.event.name;
        return this.meeting ? userNotification.meeting.id === this.meeting.id : userNotification;
      });

      this.data = {
        data: userNotifications,
        search: true,
        columns: [
          { name: 'subject', label: 'Asunto' },
          { name: 'event', label: 'Evento' },
          { name: 'createdAt', label: 'Creado', type: 'date', format: 'dd/MM/yy --- hh:mm a' },
        ],
        load: true,
        displayColumns: ['subject', 'event', 'createdAt'],
      };
    });
  }

  ngOnChanges(){
    if(this.userService.user && (this.user || this.meeting)){
     this.getUserNotifications();
    }
  }

  close(drawer: MatDrawer) {
    drawer.toggle();
  }

  openForm(event, drawer: MatDrawer) {
    this.notification = {
      subject: event.subject,
      body: event.body,
    };
    drawer.toggle();
  }
}
