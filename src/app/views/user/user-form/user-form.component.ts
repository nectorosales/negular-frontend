import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit, OnChanges {

  @Input() user: any;
  @Input() meeting: any;
  @Input() export: boolean;

  constructor(private route: ActivatedRoute, public userService: UserService) {}

  getUser(){
    if(!this.user) {
      this.route.params.subscribe(param => {
        if (param.id) {
          this.userService.show(param.id).subscribe(user => {
            console.log(user)
            this.user = user;
          });
        }
      });
    }
  }

  ngOnInit(): void {
    this.getUser();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getUser();
  }

}
