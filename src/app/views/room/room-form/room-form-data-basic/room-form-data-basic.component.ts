import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoomService} from '../../../../services/room/room.service';

@Component({
  selector: 'app-room-form-data-basic',
  templateUrl: './room-form-data-basic.component.html',
  styleUrls: ['./room-form-data-basic.component.css']
})
export class RoomFormDataBasicComponent implements OnInit {

  angForm: FormGroup;
  @Input() room;

  constructor(private fb: FormBuilder, private roomService: RoomService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      code: ['', Validators.required],
      active: [false, Validators.required],
    });
  }

  getRooms() {
    setTimeout(() => {
      if (this.room) {
        this.angForm.controls['id'].setValue(this.room.id);
        this.angForm.controls['name'].setValue(this.room.name);
        this.angForm.controls['code'].setValue(this.room.code);
        this.angForm.controls['active'].setValue(this.room.active);
      }
    }, 500);
  }

  ngOnInit() {
    this.getRooms();
  }

}
