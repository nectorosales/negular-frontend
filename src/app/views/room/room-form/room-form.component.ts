import {Component, OnInit, ViewChild} from '@angular/core';
import {MatVerticalStepper} from '@angular/material/stepper';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {RoomFormDataBasicComponent} from './room-form-data-basic/room-form-data-basic.component';
import {RoomService} from '../../../services/room/room.service';
import * as moment from 'moment';
import {MeetingService} from '../../../services/meeting/meeting.service';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-room-form',
  templateUrl: './room-form.component.html',
  styleUrls: ['./room-form.component.css']
})
export class RoomFormComponent implements OnInit {

  @ViewChild('stepper') private stepper: MatVerticalStepper;
  @ViewChild('roomFormDataBasicComponent') roomFormDataBasicComponent;
  room: any = {};
  load: boolean = false;
  private data: {
    contentDialog: string;
    search: boolean;
    displayColumns: string[];
    data: any[];
    load: boolean;
    columns: (
      | { name: string; label: string; format?: string }
      | { name: string; label: string; type: string }
      )[];
  };
  @ViewChild('tableComponent') tableComponent;

  constructor(private dialog: MatDialog, private meetingsService: MeetingService, private route: ActivatedRoute, private router: Router, private roomService: RoomService, public userService: UserService) {
    this.roomFormDataBasicComponent = RoomFormDataBasicComponent;
  }

  openForm(meeting: any) {
    this.router.navigate(['/admin/reuniones/' + meeting.id]);
  }

  getMeetings(room) {
    setTimeout(() => {
      this.meetingsService.indexByRoom(room.id).subscribe((meetings: any[]) => {
        meetings = meetings.map(m => {
          m.date = moment(m.start).toDate();
          m.users = m.users.length;
          m.name = m.event.name
          return m;
        });
        this.data = {
          data: meetings,
          search: true,
          columns: [
            { name: 'name', label: 'Evento' },
            { name: 'date', label: 'Fecha', type: 'date', format: 'dd/MM/yy' },
            { name: 'start', label: 'Inicio', type: 'date', format: 'hh:mm a' },
            { name: 'end', label: 'Fín', type: 'date', format: 'hh:mm a' },
            { name: 'users', label: 'Nº Participantes' }
          ],
          load: true,
          displayColumns: ['select', 'name', 'date', 'start', 'end', 'users'],
          contentDialog:
            '¿Estas seguro de eliminarlo? <br> Se borraran todas las reuniones asociadas al evento.'
        };
      });
    }, 500);
  }

  nextStep() {
    setTimeout(() => {
      this.stepper.next();
    }, 300);
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      if (param.id !== 'crear') {
        this.roomService.show(param.id).subscribe(room => {
          this.room = room;
          this.load = true;
        });
      } else {
        this.load = true;
      }
    });
    /*setTimeout(() => {
      this.getMeetings(this.room);
    }, 500);*/
  }

  submit() {
    this.roomFormDataBasicComponent.angForm.value.user = this.userService.user;
    this.roomService.store(this.roomFormDataBasicComponent.angForm.value).subscribe();
    this.router.navigate(['/admin/salas'])
  }

  destroy(selection) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: 'Atención', content: '¿Estas seguro de eliminarlos?<br><small>Se eliminarán los datos de forma permanente</small>' },
      disableClose: true,
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const ids = [];
        selection.forEach(obj => {
          ids.push(obj.id);
        });
        this.meetingsService.destroySelected(ids).subscribe( () => {
          this.getMeetings(this.room);
          this.tableComponent.selection.clear();
        });
      }
    });
  }
}
