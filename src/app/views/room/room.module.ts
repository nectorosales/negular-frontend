import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomListComponent } from './room-list/room-list.component';
import {RoomComponent} from './room.component';
import {RoomRoutingModule} from './room-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { RoomFormComponent } from './room-form/room-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import { RoomFormDataBasicComponent } from './room-form/room-form-data-basic/room-form-data-basic.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [RoomComponent, RoomListComponent, RoomFormComponent, RoomFormDataBasicComponent],
    imports: [
        CommonModule,
        RoomRoutingModule,
        SharedModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatStepperModule,
        MatTooltipModule,
        MatSlideToggleModule
    ]
})
export class RoomModule { }
