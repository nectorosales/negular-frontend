import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {RoomService} from '../../../services/room/room.service';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  data = { data: [], search: false, columns: [], displayColumns: [], load: false };

  constructor(private dialog: MatDialog, private router: Router, public roomService: RoomService, public userService: UserService) {}

  ngOnInit() {
    this.getRooms();
  }

  getRooms(){
    setTimeout(() => {
      if(this.userService.user) {
        this.roomService.indexByUser(this.userService.user.id).subscribe((data: any[]) => {
          this.data = {
            data: data,
            search: true,
            columns: [
              { name: 'name', label: 'Nombre' },
              { name: 'code', label: 'Código' },
              {
                name: 'createdAt',
                label: 'Creado',
                type: 'date'
              }
            ],
            load: true,
            displayColumns: [
              'select',
              'name',
              'code',
              'createdAt',
            ]
          };

        });
      }
    }, 500);
  }

  openForm(obj) {
    this.router.navigate(['/admin/salas/' + obj.id]);
  }


  destroy(selection) {
    const ids = [];
    selection.forEach(obj => {
      ids.push(obj.id);
    });
    this.roomService.destroySelected(ids).subscribe( () => {
      this.getRooms();
    });
  }

}
