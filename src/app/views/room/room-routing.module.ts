import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {RoomListComponent} from './room-list/room-list.component';
import {RoomFormComponent} from './room-form/room-form.component';

const routes: Routes = [
  {
    path: '',
    component: RoomListComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
  {
    path: ':id',
    component: RoomFormComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomRoutingModule {}
