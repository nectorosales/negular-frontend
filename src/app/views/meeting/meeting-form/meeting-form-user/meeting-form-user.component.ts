import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../../services/shared/global.service';

@Component({
  selector: 'app-meeting-user-form',
  templateUrl: './meeting-form-user.component.html',
  styleUrls: ['./meeting-form-user.component.css']
})
export class MeetingFormUserComponent implements OnChanges, OnInit {

  @Input() event;
  @Input('someProp') someProp;
  public angForm: FormGroup;
  questions: any[] = [];
  options: any[] = [];

  constructor(private fb: FormBuilder, private globalService: GlobalService) {
    this.createForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes) {
      this.createForm();
    }
  }

  formatLabel = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().replace(' ', '-');
  }

  private createForm() {
    let gf = {};
    this.event?.questions?.map(question => {
      let fcn = question.type === 'name' ? 'name' : question.type === 'lastName' ? 'lastName' : question.type === 'phone' ? 'phone' : question.type === 'email' ? 'email' : this.formatLabel(question.label);;
      let validator = question.required && question.type === 'email' ? [Validators.required, Validators.email] : question.required ? Validators.required : '';
      gf[fcn] = ['', validator];
      question.fcn = fcn;
      if(question.options.length > 0){
        question.options = question.options.map(option => {
          option.checked = false;
          return option;
        })
      }
      return question;
    });
    this.angForm = this.fb.group(gf);
    if(this.event && this.event.questions){
      this.questions = this.globalService.sortBy(this.event.questions, 'order');
    }
  }


  onChange(event, option, question) {
    option.checked = event.checked;
    let checkedOptions = question.options.filter(o => o.checked);
    let valueOptions = [];
    checkedOptions.forEach(o => valueOptions.push(o.label));
    this.angForm.controls[question.fcn].setValue(valueOptions);
  }

  ngOnInit(): void {
    if(this.someProp) {
      setTimeout(() => {
        this.event = this.someProp;
        this.createForm();
      }, 500)
    }
  }
}
