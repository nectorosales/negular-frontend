import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { EventService } from '../../../../services/event/event.service';
import { UserService } from '../../../../services/user/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import {MeetingService} from '../../../../services/meeting/meeting.service';
import {SnackBarService} from '../../../../services/shared/snackbar.service';

@Component({
  selector: 'app-select-event-form',
  templateUrl: './select-event-form.component.html',
  styleUrls: ['./select-event-form.component.css']
})
export class SelectEventFormComponent implements OnInit, OnChanges {
  @Output() emitForm = new EventEmitter();
  events: any[] = [];
  statesMeeting: any[] = [];
  angForm: FormGroup;
  @Input() meeting;
  disabledList: boolean = false;

  constructor(
    private eventService: EventService,
    public userService: UserService,
    private fb: FormBuilder,
    private meetingService: MeetingService,
    private snackBarService: SnackBarService
  ) {
    this.createForm();
    this.statesMeeting = [
      { label: 'Pendiente', value: 'pending' },
      { label: 'Cancelado', value: 'cancelled' },
      { label: 'Finalizado', value: 'finished' }
    ];
  }

  selected(event) {
    this.angForm.controls['event'].setValue(event);
    this.emitForm.emit(this.angForm.value);
  }

  ngOnInit(): void {
    setTimeout(() => {
      if (this.userService.user) {
        this.eventService
          .indexByUser(this.userService.user.id)
          .subscribe((events: any[]) => {
            this.events = events;
          });
      }
    }, 800);
  }

  private createForm() {
    this.angForm = this.fb.group({
      event: ['', Validators.required]
    });
  }

  getEvent() {
    this.eventService.show(this.meeting?.event?.id).subscribe((event: any) => {
      if(event) {
        this.meeting.event.img = environment.apiEndpointPublic + event.img;
        this.angForm.controls['event'].setValue(event);
        this.disabledList = true;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && this.meeting?.event) {
      this.getEvent();
    }
  }

  activeMeeting() {
    this.meeting.active = !this.meeting.active;
    this.meetingService.update(this.meeting).subscribe((meeting: any) => {
      if (meeting.active) {
        this.snackBarService.showAlert('La sala ha sido activada');
      } else {
        this.snackBarService.showAlert('La sala ha sido desactivada');
      }
    });
  }


  changeStateMeeting(pending: string) {

  }

  openMeetRoom() {
    window.open(environment.jitsiMeet + this.meeting?.id, '_blank');
  }
}
