import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import { MatVerticalStepper } from '@angular/material/stepper';
import { SelectDateAvailableFormComponent } from './select-date-available-form/select-date-available-form.component';
import { SelectEventFormComponent } from './select-event-form/select-event-form.component';
import { MeetingFormUserComponent } from './meeting-form-user/meeting-form-user.component';
import { MeetingService } from '../../../services/meeting/meeting.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { GlobalService } from '../../../services/shared/global.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user/user.service';
import { TableComponent } from '../../../shared/table/table.component';
import { AuthService } from '../../../services/auth/auth.service';
import { Roles } from '../../../enums/Roles';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { MatDrawer, MatSidenav } from '@angular/material/sidenav';
import { SnackBarService } from '../../../services/shared/snackbar.service';
import { RoomService } from '../../../services/room/room.service';
import { NotificationService } from '../../../services/notification/notification.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationFormDataBasicComponent } from '../../notification/notification-form/notification-form-data-basic/notification-form-data-basic.component';
import {UserMeetingService} from '../../../services/user-meeting/user-meeting.service';

@Component({
  selector: 'app-meeting-form',
  templateUrl: './meeting-form.component.html',
  styleUrls: ['./meeting-form.component.css']
})
export class MeetingFormComponent implements OnInit {
  @ViewChild('stepper') private stepper: MatVerticalStepper;
  @ViewChild('drawerMeetingNotification')
  private drawerMeetingNotification: MatSidenav;
  @ViewChild('drawerMeetingUsers') private drawerMeetingUsers: MatSidenav;
  meeting: any = {};
  public Editor = ClassicEditor;
  @ViewChild('formSelectEventForm')
  public formSelectEventForm: SelectEventFormComponent;
  @ViewChild('formSelectDateAvailable')
  public formSelectDateAvailable: SelectDateAvailableFormComponent;
  @ViewChild('formUserForm') public formUserForm: MeetingFormUserComponent;
  dataTable = {
    data: [],
    search: false,
    columns: [],
    displayColumns: [],
    load: false,
    contentDialog: ''
  };
  @ViewChild('tableComponent') tableComponent;
  load: boolean = false;
  descriptionEmail: string;
  users: any[] = [];
  rooms: any[] = [];
  angForm: FormGroup;
  notifications: any[] = [];
  @ViewChild('notificationFormDataBasicComponent')
  private notificationFormDataBasicComponent: NotificationFormDataBasicComponent;
  user: any;
  @Output() emitClick = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private roomService: RoomService,
    private snackBarService: SnackBarService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private meetingService: MeetingService,
    private globalService: GlobalService,
    private dialog: MatDialog,
    private router: Router,
    private userMeetingService: UserMeetingService,
    public userService: UserService
  ) {
    this.tableComponent = TableComponent;
    this.createForm();
  }

  nextStep(event) {
    this.stepper.next();
  }

  getNotifications(userId) {
    this.notificationService
      .indexByUser(userId)
      .subscribe((notifications: any[]) => {
        this.notifications = notifications;
      });
  }

  createForm() {
    this.angForm = this.fb.group({
      notification: [null, Validators.required]
    });
  }

  generateUser(user = null) {
    let createUser = {
      email: '',
      name: '',
      lastName: '',
      phone: '',
      password: 'negular', //Math.floor(Math.random() * 9999).toString()
      role: '',
      state_payment: 'pending',
    };
    let sortForm = this.globalService.sortBy(
      Object.keys(this.formUserForm.angForm.value),
      'desc'
    );
    sortForm.map(key => {
      if (key === 'email') {
        createUser.email = user
          ? user.email
          : this.formUserForm.angForm.value['email']
          ? this.formUserForm.angForm.value['email']
          : null;
        createUser.name = user
          ? user.name
          : this.formUserForm.angForm.value['name']
          ? this.formUserForm.angForm.value['name']
          : null;
        createUser.lastName = user
          ? user.lastName
          : this.formUserForm.angForm.value['lastName']
          ? this.formUserForm.angForm.value['lastName']
          : null;
        // @ts-ignore
        createUser.phone = user
          ? user.phone
          : this.formUserForm.angForm.value['phone']
          ? parseInt(this.formUserForm.angForm.value['phone'])
          : null;
        createUser.role = Roles.CLIENT;
      }
      this.descriptionEmail +=
        '<strong>' +
        key +
        ': </strong>' +
        this.formUserForm.angForm.value[key] +
        '<br>';
    });
    return createUser;
  }

  storeOrUpdateUser(user = null) {
    const createUser = this.generateUser(user);
    this.userService.storeOrUpdate(createUser).subscribe((u : any)=> {
      if (!user) {
        this.createOuUpdateMeeting(u);
      } else {
        this.users.push(u);
      }
      //sendEmail
    });
  }

  /*getRooms(){
    this.roomService.indexByUser(this.userService.user.id).subscribe( (rooms: any[]) => {
      this.rooms = rooms;
    });
  }*/

  addUser() {
    const user = this.generateUser();
    this.users.push(user);
    this.getUsers();
    this.drawerMeetingUsers.close();
    this.formUserForm.angForm.reset();
  }

  createOuUpdateMeeting(user = null) {
    const meeting = {
      id: this.meeting?.id,
      event: this.formSelectEventForm.angForm.value.event.id,
      start: this.formSelectDateAvailable.angForm.value.date.start,
      end: this.formSelectDateAvailable.angForm.value.date.end,
      active: this.meeting?.active,
      state: this.meeting?.state,
      description: this.meeting?.description ? this.meeting?.description : '',
    };
    if (!this.meeting.id) {
      delete meeting.id;
    }
    if (this.meeting.id) {
      this.meetingService.update(meeting).subscribe( () => {
        this.getMeetingsDrawer(this.userService.user.id);
      });
    } else {
      this.meetingService.store(meeting).subscribe( (m: any) => {
        this.userMeetingService
          .store({ user: user.id, meeting: m.id })
          .subscribe( () => {
            this.getMeetingsDrawer(user.id);
          });
      });
    }
  }

  getMeetingsDrawer(id) {
    this.meetingService.getMeetingRoomByUser(id).subscribe( (meetings: any[]) => {
      this.meetingService.setMeetings(meetings);
    });
  }

  onSubmit() {
    this.descriptionEmail = '';
    if (!this.meeting.id) {
      this.storeOrUpdateUser();
      this.router.navigate(['/admin/reuniones']);
    } else {
      if (this.users.length > 0) {
        this.users.forEach((u) => {
          if(!u.id) this.storeOrUpdateUser(u);
        });
        setTimeout(() => {
          const users = this.users.filter(u => u.id);
          var onlyInA = this.meeting.userMeetings.filter(
            this.globalService.comparer(users, 'email')
          );
          var onlyInB = users.filter(
            this.globalService.comparer(this.meeting.userMeetings, 'email')
          );
          let result = onlyInA.concat(onlyInB);
          if (result.length > 0) {
            result.forEach(u => {
              if (u.id) {
                this.userMeetingService
                  .store({ user: u.id, meeting: this.meeting.id })
                  .subscribe();
              }
            });
          }
          this.createOuUpdateMeeting();
          //this.router.navigate(['/admin/reuniones']);
          this.emitClick.emit(true);
        }, 500);
      } else {
        this.snackBarService.showAlert(
          'Debes asignar al menos un participante'
        );
      }
    }
  }

  ngOnInit() {
    this.route.params.subscribe(param => {
      if (param.id !== 'crear') {
        this.meetingService.show(param.id).subscribe((meeting: any) => {
          this.meeting = meeting;
          this.users = Object.assign(
            [],
            this.meeting?.userMeetings?.map(userMeeting => {
              userMeeting.userMeetingId = userMeeting.id;
              userMeeting.id = userMeeting.user.id;
              userMeeting.email = userMeeting.user.email;
              return userMeeting;
            })
          );
          this.load = true;
        });
      } else {
        this.load = true;
      }
      //this.getMeetings(this.event)
    });
    setTimeout(() => {
      if (this.userService.user) {
        this.getUsers();
        this.getNotifications(this.userService.user.id);
      }
    }, 800);

    //this.getRooms();
  }

  getUsers() {
    this.dataTable = {
      data: this.users,
      search: true,
      columns: [{ name: 'email', label: 'Email' }, { name: 'state_payment', label: 'Pago' }],
      load: true,
      displayColumns: ['select', 'email', 'state_payment'],
      contentDialog: '¿Estas seguro de eliminarlo?'
    };
  }

  destroy(selection) {
    if (selection.selected.length > 0) {
      const dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Atención',
          content:
            '¿Estas seguro de eliminarlos?<br><small>Los cambios se actualizarán automáticamente</small>'
        },
        disableClose: true,
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let ids = [];
          selection?.selected?.forEach(user => {
            ids.push(user.userMeetingId);
            this.users = this.users.filter(u => {
              return u.email !== user.email;
            });
          });
          this.userMeetingService.destroySelected(ids).subscribe();
          this.meeting.userMeetings = this.users;
          this.getUsers();
          this.tableComponent.selection.clear();
        }
      });
    } else {
      this.snackBarService.showAlert(
        'Debes seleccionar algún elemento del listado'
      );
    }
  }

  destroyMeeting() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: 'Atención', content: '¿Estas seguro de eliminarlo?' },
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.meetingService.destroy(this.meeting.id).subscribe(() => {
          this.getMeetingsDrawer(this.userService.user.id);
          this.router.navigate(['/admin/reuniones']);
        });
      }
    });
  }

  openForm(user: any) {
    this.user = user;
    this.drawerMeetingNotification.toggle();
    //this.router.navigate(['/admin/usuarios/' + user.id]);
  }

  openNotificationForm(selected) {
    this.user = null;
    if (selected.length > 0) {
      this.drawerMeetingNotification.toggle();
    } else {
      this.snackBarService.showAlert(
        'Debes seleccionar algún elemento del listado'
      );
    }
  }

  sendEmail() {
    let users = '<ul>';
    let removeDuplicateUsers = [];
    this.tableComponent.selection.selected.forEach(user => {
      if (!removeDuplicateUsers.includes(user) && user.email) {
        removeDuplicateUsers.push(user);
        users += '<li>' + user.email + '</li>';
      }
    });
    users += '</ul>';

    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Atención',
        content: 'Vas a enviar un Email a los siguiente usuarios:<br>' + users
      },
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.notificationFormDataBasicComponent.angForm.value.email = this.userService.user.email;
        const obj = {
          users: removeDuplicateUsers,
          notification: this.notificationFormDataBasicComponent.angForm.value,
          meeting_id: this.meeting.id
        };
        this.notificationService.sendEmail(obj).subscribe(email => {
          this.drawerMeetingNotification.close();
          this.notificationFormDataBasicComponent.angForm.reset();
          this.tableComponent.selection.clear();
          this.snackBarService.showAlert(
            'La notificación ha sido enviada correctamente'
          );
        });
      }
    });
  }

  close(drawer: MatDrawer) {
    drawer.toggle();
    this.tableComponent.selection.clear();
  }
}
