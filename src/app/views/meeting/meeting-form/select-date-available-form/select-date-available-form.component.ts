import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {SnackBarService} from '../../../../services/shared/snackbar.service';
import {MeetingService} from '../../../../services/meeting/meeting.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {MatCalendar} from '@angular/material/datepicker';

@Component({
  selector: 'app-select-date-available-form',
  templateUrl: './select-date-available-form.component.html',
  styleUrls: ['./select-date-available-form.component.css']
})
export class SelectDateAvailableFormComponent implements OnChanges {

  @Input() event;
  @Input() meeting;
  public angForm: FormGroup;
  selectedDate: any;
  timesAvailable: any[] = [];
  @Output() emitForm = new EventEmitter();
  @ViewChild(MatCalendar, {static: false}) calendar: MatCalendar<Date>;
  datesNotAvailable = [];

  constructor(private ref: ChangeDetectorRef, private snackBarService: SnackBarService, private meetingService: MeetingService, private fb: FormBuilder) {
    this.createForm();
    //ref.detach();
  }

  filterDates = (d: Date): boolean => {
    return this.filteredDatesEvent(d);
  }

  filteredDatesEvent(d){
    let day = moment(d).toDate();
    let daysEnabled = [];
    let datesEnabled = [];
    this.event?.availabilities?.forEach(availability => {
      if(availability.always) {
        daysEnabled.push(availability.day);
      }else if(availability.date){
        datesEnabled.push(availability.date);
      }
    });
    return (daysEnabled.includes(day.getDay()) || datesEnabled.includes(moment(day).format('YYYY-MM-DD'))) && day > moment().toDate();
  }

  createForm() {
    this.angForm = this.fb.group({
      date: ['', [Validators.required]],
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.event = changes.event?.currentValue;
      this.selectedDate = null;
      this.calendar?.updateTodaysDate();

      if(this.meeting?.id && this.event) {
        this.selectedDate = moment(this.meeting.start).toDate();
        this.calculateRange(this.selectedDate);
        this.angForm.controls['date'].setValue(this.meeting);
      }
    }
  }

  onSelect(event){
    this.selectedDate = event;
    this.calculateRange(this.selectedDate);
  }

  getMeetingsDisabled(selectedDate){
    this.datesNotAvailable = [];
    this.meetingService.index(this.event.id).subscribe( (events: any[]) => {
      events?.forEach(event => {
        if(moment(event.start).format('DD-MM-yyyy').toString() === moment(selectedDate).format('DD-MM-yyyy').toString()){
          this.datesNotAvailable.push(moment(event.start).toDate().getTime());
        }
      });
    });
  }

  private calculateRange(selectedDate) {
    if(this.event.typeEvent === 'single') this.getMeetingsDisabled(selectedDate);
    this.timesAvailable = [];
    setTimeout(() => {
      let dateSelected = moment(selectedDate).format('DD-MM-yyyy');
      this.event?.availabilities?.forEach(availability => {
        if(moment(selectedDate).toDate().getDay() === availability.day){
          let start = moment(dateSelected + ' ' + availability.begin,'DD-MM-yyyy HH:mm:ss').toDate();
          while(start < moment(dateSelected + ' ' + availability.end,'DD-MM-yyyy HH:mm:ss').toDate()){
            let selected = moment(this.meeting?.start).toDate().getTime() === moment(start).toDate().getTime();
            if(!this.datesNotAvailable.includes(moment(start).toDate().getTime()) || selected){
              this.timesAvailable.push({start: start, end: moment(start).add(this.event.duration, "minutes"), selected: selected})
            }
            start = moment(start).add(this.event.duration, "minutes").toDate();
          }
        }
      });
      this.ref.detectChanges();
    }, 600);
  }

  selected(selected) {
    this.angForm.controls['date'].setValue(selected);
    this.emitForm.emit(this.angForm.value);
  }
}
