import { NgModule } from '@angular/core';
import { MeetingComponent } from './meeting.component';
import {CommonModule, DatePipe, TitleCasePipe} from '@angular/common';
import { CalendarModule } from 'angular-calendar';
import { MatIconModule } from '@angular/material/icon';
import { SelectDateAvailableFormComponent } from './meeting-form/select-date-available-form/select-date-available-form.component';
import { MeetingFormUserComponent } from './meeting-form/meeting-form-user/meeting-form-user.component';
import { MeetingFormComponent } from './meeting-form/meeting-form.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MeetingRoutingModule } from './meeting-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import { SharedModule } from '../../shared/shared.module';
import { MatInputModule } from '@angular/material/input';
import { SlugifyPipe } from '../../pipes/slugify.pipe';
import { SelectEventFormComponent } from './meeting-form/select-event-form/select-event-form.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  SatDatepickerModule,
  SatNativeDateModule
} from 'saturn-datepicker';
import {
  MAT_MOMENT_DATE_FORMATS, MatMomentDateModule,
  MomentDateAdapter
} from '@angular/material-moment-adapter';
import {MatTabsModule} from '@angular/material/tabs';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatNativeDateModule} from '@angular/material/core';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NotificationModule} from '../notification/notification.module';
import {UserModule} from '../user/user.module';

@NgModule({
  declarations: [
    MeetingComponent,
    SelectDateAvailableFormComponent,
    MeetingFormUserComponent,
    MeetingFormComponent,
    MeetingListComponent,
    SelectEventFormComponent,
  ],
  imports: [
    CommonModule,
    CalendarModule,
    MatIconModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatStepperModule,
    MatRadioModule,
    MeetingRoutingModule,
    MatButtonModule,
    SharedModule,
    MatInputModule,
    MatCheckboxModule,
    MatListModule,
    SatDatepickerModule,
    SatNativeDateModule,
    MatTabsModule,
    NgxPaginationModule,
    MatExpansionModule,
    MatSidenavModule,
    MatTooltipModule,
    MatSelectModule,
    MatNativeDateModule,
    MatMomentDateModule,
    CKEditorModule,
    FormsModule,
    MatSlideToggleModule,
    NotificationModule,
    UserModule,
  ],
  exports: [
    SelectDateAvailableFormComponent,
    MeetingFormUserComponent
  ],
  providers: [
    SlugifyPipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    DatePipe,
    TitleCasePipe
  ]
})
export class MeetingModule {}
