import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {MeetingFormComponent} from './meeting-form/meeting-form.component';

const routes: Routes = [
  {
    path: '',
    component: MeetingListComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
  {
    path: ':id',
    component: MeetingFormComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeetingRoutingModule {}
