import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK
} from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MeetingService } from '../../../services/meeting/meeting.service';
import { UserService } from '../../../services/user/user.service';
import {EventComponent} from '../../event/event.component';
import {PathService} from '../../../services/shared/path.service';
import {EventAction, EventColor} from 'calendar-utils';
import {EventService} from '../../../services/event/event.service';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';

export interface CalendarEvent<MetaType = any> {
  id?: string | number;
  event?: number;
  start: Date;
  end?: Date;
  title: string;
  typeEvent?: string;
  description?: string;
  users?: any[];
  color?: EventColor;
  actions?: EventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
  meta?: MetaType;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class MeetingListComponent implements OnInit {
  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  activeDayIsOpen: boolean = true;
  eventComponent;
  meeting: any;
  legend: any[] = [];
  meetings: any[] = [];

  constructor(
    private dialog: MatDialog,
    private meetingService: MeetingService,
    public userService: UserService,
    private pathService: PathService,
    private eventService: EventService,
    private router: Router
  ) {
    this.eventComponent = EventComponent;
  }

  getMeetings(){
    this.events = [];
    setTimeout(() => {
      if (this.userService.user) {
        this.meetingService
          .indexByUser(this.userService.user.id)
          .subscribe((meetings: CalendarEvent[]) => {
            meetings.forEach((m: any) => {
              let users = '';
              m.userMeetings?.forEach( u => {
                users += u.user?.email + '<br>';
              });

              this.events.push({
                id: m.id,
                event: m.event,
                title: '<strong>' + m.event.name + '</strong><br>' + users,
                users: m.users,
                start: new Date(m.start),
                end: new Date(m.end),
                description: m.description,
                color: {
                  primary: m.event.colour,
                  secondary: 'transparent',
                },
                resizable: {
                  beforeStart: true,
                  afterEnd: true,
                },
                draggable: false,
              });
            });
            this.refresh.next();
          });
      }
    }, 1000);
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.router.navigate(['/admin/reuniones/' + event.id]);
  }

  getMeetingByUser(){
    setTimeout(() => {
     if(this.userService.user){
        this.eventService.indexByUser(this.userService.user.id).subscribe( (events: any[]) => {
          this.legend = events;
        });
      }
    }, 1200);
  }

  ngOnInit() {
      this.getMeetings();
      if(this.userService.user) {
        this.getMeetingByUser();
      }
  }
}
