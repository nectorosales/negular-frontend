import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileComponent} from './profile/profile.component';
import {SettingComponent} from './setting.component';
import {SettingRoutingModule} from './setting-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [ProfileComponent, SettingComponent],
    imports: [
        CommonModule,
        SettingRoutingModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatDividerModule,
        MatInputModule,
        MatButtonModule,
        MatSlideToggleModule,
        SharedModule
    ]
})
export class SettingModule { }
