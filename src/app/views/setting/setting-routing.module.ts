import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {ProfileComponent} from './profile/profile.component';
import {SettingComponent} from './setting.component';

const routes: Routes = [
  {
    path: '',
    component: SettingComponent ,
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        redirectTo: 'mi-perfil',
        pathMatch: 'full'
      },
      {
        path: 'mi-perfil',
        canActivate: [AuthGuardService],
        canActivateChild: [RoleGuardService],
        data: { roles: [Roles.USER] },
        component: ProfileComponent
      },
      {
        path: 'cambiar-clave',
        canActivate: [AuthGuardService],
        canActivateChild: [RoleGuardService],
        data: { roles: [Roles.USER] },
        component: ProfileComponent,
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule {}
