import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user/user.service';
import {FileHandle} from '../../../directives/dragDrop.directive';
import {NgxImageCompressService} from 'ngx-image-compress';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  angForm: FormGroup;
  files: FileHandle[] = [];
  loadFile;

  filesDropped(files: FileHandle[]): void {
    this.loadFile = files[0].url;
    const reader = new FileReader();
    reader.readAsDataURL(files[0].file);
    reader.onload = () => {
      this.compressImage(reader.result, null);
    };
  }


  constructor(private fb: FormBuilder, public userService: UserService, public imageCompress: NgxImageCompressService) {
    this.createForm();
  }

  getUser(){
    this.loadFile = this.userService.user.details?.img ? environment.apiEndpointPublic + this.userService.user.details.img : null;
    this.angForm.controls['img'].setValue(this.userService.user.details.img);
    this.angForm.controls['email'].setValue(this.userService.user.email);
    this.angForm.controls['email'].disable();
    this.angForm.controls['name'].setValue(this.userService.user.details.name);
    this.angForm.controls['lastName'].setValue(this.userService.user.details.lastName);
    this.angForm.controls['phone'].setValue(this.userService.user.details.phone);
    this.angForm.controls['activeBizum'].setValue(this.userService.user.details.activeBizum);
  }

  ngOnInit(): void {
    setTimeout(() => {
      if(this.userService.user) this.getUser();
    }, 1500)
  }

  createForm() {
    this.angForm = this.fb.group({
      img: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      name: [''],
      lastName: [''],
      phone: [''],
      activeBizum: [false],
    });
  }

  compressFile() {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.compressImage(image, orientation);
    });
  }

  compressImage(image, orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        this.loadFile = result; //base64
        const file = this.convertDataURIToFile(result);
        this.angForm.controls['img'].setValue(file);
        console.log(this.angForm.value, this.angForm.valid)
        console.warn('Size in KB is now:', this.imageCompress.byteCount(result)/1024);
      }
    );
  }

  convertDataURIToFile(dataurl) {
    let arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], 'fileName.' + mime.split('/')[1], {type:mime});
  }

  onSubmit() {
    this.angForm.value.id = this.userService.user.id;
    this.angForm.value.email = this.userService.user.email;
    this.angForm.value.activeBizum = this.angForm.value.activeBizum ? 1 : 0;
    this.userService.update(this.angForm.value).subscribe(user => {
      this.userService.setUser(user);
    });

  }
}
