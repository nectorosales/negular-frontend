import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EventService} from '../../../services/event/event.service';
import {PathService} from '../../../services/shared/path.service';
import {MatVerticalStepper} from '@angular/material/stepper';
import {environment} from '../../../../environments/environment';
import {SelectDateAvailableFormComponent} from '../../meeting/meeting-form/select-date-available-form/select-date-available-form.component';
import {MeetingFormUserComponent} from '../../meeting/meeting-form/meeting-form-user/meeting-form-user.component';
import {GlobalService} from '../../../services/shared/global.service';

@Component({
  selector: 'app-username-event-form',
  templateUrl: './username-event-form.component.html',
  styleUrls: ['./username-event-form.component.css']
})
export class UsernameEventFormComponent implements OnInit {

  @ViewChild('stepper') public stepper: MatVerticalStepper;
  @ViewChild('formSelectDateAvailable') public formSelectDateAvailable: SelectDateAvailableFormComponent;
  @ViewChild('formUserForm') public formUserForm: MeetingFormUserComponent;
  event: any;
  meeting: any;
  load: boolean = false;
  selectedDateTime: any = { start: null, end: null};

  constructor(private route: ActivatedRoute, private eventService: EventService, public pathService: PathService, private globalService: GlobalService) { }

  getEvent(){
    this.route.params.subscribe(params => {
      this.eventService.getPublicEvent(params.username, params.event).subscribe( (event: any) => {
        this.event = event;
        this.event.img = environment.apiEndpointPublic + event.img;
        this.load = true;
      })
    });
  }

  nextStep(event) {
    this.selectedDateTime.start = event.date?.start;
    this.selectedDateTime.end = event.date?.end;
    this.stepper.next();
  }

  ngOnInit(): void {
    this.pathService.isHome = true;
    this.getEvent();
  }

  backClicked() {
    this.globalService.backClicked();
  }

}
