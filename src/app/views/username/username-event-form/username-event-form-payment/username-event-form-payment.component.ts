import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-username-event-form-payment',
  templateUrl: './username-event-form-payment.component.html',
  styleUrls: ['./username-event-form-payment.component.css']
})
export class UsernameEventFormPaymentComponent implements OnInit {

  @Input() email: string;
  @Input() price: string;

  constructor() { }

  ngOnInit(): void {
  }

}
