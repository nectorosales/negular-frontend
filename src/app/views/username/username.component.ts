import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import Typed from 'typed.js';
import {PathService} from '../../services/shared/path.service';
import {SlugifyPipe} from '../../pipes/slugify.pipe';
import {GlobalService} from '../../services/shared/global.service';

@Component({
  selector: 'app-username',
  templateUrl: './username.component.html',
  styleUrls: ['./username.component.css']
})
export class UsernameComponent implements OnInit {
  user: any;
  angForm: FormGroup;
  webinars: any[] = [];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    public pathService: PathService,
    public globalService: GlobalService
  ) {
    this.createForm();
  }

  getUser() {
    this.route.params.subscribe(param => {
      this.userService.getUsername(param.username).subscribe(
        (user: any) => {
          this.user = user;
          //this.webinars = [];
          this.user.events = this.user.events.filter(event => {
            event.src = environment.apiEndpointPublic + event.img;
           /* if(!!event.availabilities.filter(a => !a.always)[0]) {
              event.date = event.availabilities.filter(a => !a.always)[0].date;
              this.webinars.push(event);
            }
            if(!!event.availabilities.filter(a => a.always)[0]) {
              return event;
            }*/
           return event.active;
          });
          //this.webinars = this.globalService.sortBy(this.webinars, 'date');
          this.user.logo = environment.apiEndpointPublic + user?.details?.img;
        },
        () => {
          this.router.navigate(['/404']);
        }
      );
    });
  }

  openRoom() {
    if (this.angForm.valid) {
      location.href =
        environment.jitsiMeet + this.angForm.controls['room'].value;
    }
  }

  createForm() {
    this.angForm = this.fb.group({
      room: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.pathService.isHome = true;
    this.getUser();
    const options = {
      strings: ['Hola, usuario', 'Estos son mis servicios disponibles...'],
      typeSpeed: 50,
      backSpeed: 40,
      showCursor: true,
      cursorChar: '|',
      loop: false
    };

    //const typed = new Typed('.typed-element', options);
  }

  openSheet(event: any) {
    //this._bottomSheet.open();
  }

  openEvent(event) {
    this.route.params.subscribe(params => {
      this.router.navigate(['/' + params.username + '/' + event.slug]);
    })
  }
}

