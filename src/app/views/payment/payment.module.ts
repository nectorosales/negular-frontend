import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PaymentComponent} from './payment.component';
import { PaymentListComponent } from './payment-list/payment-list.component';
import {PaymentRoutingModule} from './payment-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [PaymentComponent, PaymentListComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule
  ]
})
export class PaymentModule { }
