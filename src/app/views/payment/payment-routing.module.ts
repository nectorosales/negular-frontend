import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {PaymentListComponent} from './payment-list/payment-list.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentListComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule {}
