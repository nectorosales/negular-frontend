import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {UserMeetingService} from '../../../services/user-meeting/user-meeting.service';
import {UserService} from '../../../services/user/user.service';
import {TableComponent} from '../../../shared/table/table.component';
import * as moment from 'moment';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../../services/shared/snackbar.service';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.css']
})
export class PaymentListComponent implements OnInit {

  data = { data: [], search: false, columns: [], displayColumns: [], load: false };
  @ViewChild('tableComponent') tableComponent;

  constructor(private snackBarService: SnackBarService, private dialog: MatDialog, private router: Router, private userMeetingService: UserMeetingService, public userService: UserService) {
    this.tableComponent = TableComponent;
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    setTimeout(() => {
      if(this.userService.user) {
        this.userMeetingService.payments(this.userService.user.id).subscribe((userMeetings: any[]) => {

          userMeetings = userMeetings.filter( (userMeeting: any) => {
            userMeeting.email = userMeeting.user.email;
            userMeeting.event = userMeeting.meeting.event.name;
            userMeeting.price = userMeeting.meeting.event.price + ' €';
            userMeeting.start = moment(userMeeting.meeting.start);
            return userMeeting;
          });

          this.data = {
            data: userMeetings,
            search: true,
            columns: [
              { name: 'email', label: 'Usuario' },
              { name: 'event', label: 'Evento' },
              { name: 'price', label: 'Precio' },
              { name: 'state_payment', label: 'Pago' },
              { name: 'start', label: 'Comienzo', type: 'date', format: 'dd/MM/yy --- HH:mm a' },
            ],
            load: true,
            displayColumns: ['select', 'email', 'event', 'price', 'state_payment', 'start'],
          };

        });
      }
    }, 500);
  }

  openForm(event) {
    this.router.navigate(['/admin/usuarios/' + event.user.id])
  }

  changeStatePayment(selected: any[], statePayment) {
    if (selected.length > 0) {
      const dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Atención',
          content:
            '¿Estas seguro de cambiar su estado a <strong>' + statePayment + '</strong> ?'
        },
        disableClose: true,
        autoFocus: false
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          selected?.forEach(userMeeting => {
            userMeeting.state_payment = statePayment;
            const obj = {
              id: userMeeting.id,
              user: userMeeting.user.id,
              meeting: userMeeting.meeting.id,
              state_payment: userMeeting.state_payment,
            }
            this.userMeetingService.update(obj).subscribe();
          });
          this.snackBarService.showAlert('Se ha actualizado correctamente');
          this.tableComponent.selection.clear();
        }
      });
    } else {
      this.snackBarService.showAlert(
        'Debes seleccionar algún elemento del listado'
      );
    }
  }
}
