import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import {FileHandle} from '../../../../directives/dragDrop.directive';
import {environment} from '../../../../../environments/environment';
import {NgxImageCompressService} from 'ngx-image-compress';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {UserService} from '../../../../services/user/user.service';

@Component({
  selector: 'app-event-form-data-basic',
  templateUrl: './event-form-data-basic.component.html',
  styleUrls: ['./event-form-data-basic.component.css']
})
export class EventFormDataBasicComponent implements OnChanges {
  angForm: FormGroup;
  @Input() data;
  files: FileHandle[] = [];
  public Editor = ClassicEditor;
  loadFile;

  filesDropped(files: FileHandle[]): void {
    this.loadFile = files[0].url;
    const reader = new FileReader();
    reader.readAsDataURL(files[0].file);
    reader.onload = () => {
      this.compressImage(reader.result, null);
    };
  }

  public color: ThemePalette = 'primary';
  public touchUi = false;

  constructor(private fb: FormBuilder, public imageCompress: NgxImageCompressService, public userService: UserService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      id: [''],
      img: ['', Validators.required],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      typeEvent: ['', Validators.required],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      colour: ['', Validators.required],
      duration: ['', Validators.required],
      price: [0, Validators.required],
      active: [false]
    });
  }

  compressFile() {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.compressImage(image, orientation);
    });
  }

  compressImage(image, orientation) {

   /* let i = 50;
    let sizeFile = Math.trunc(this.imageCompress.byteCount(image)/1024);
    console.warn('Size in KB was:', this.imageCompress.byteCount(image)/1024);

    while(sizeFile > 250) {
      sizeFile = Math.round(sizeFile/2);
      i = Math.round(i/2);
    }*/
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        this.loadFile = result; //base64
        const file = this.convertDataURIToFile(result);
        this.angForm.controls['img'].setValue(file);
        console.log(this.angForm.value, this.angForm.valid)
        console.warn('Size in KB is now:', this.imageCompress.byteCount(result)/1024);
      }
    );
  }

  convertDataURIToFile(dataurl) {
    let arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], 'fileName.' + mime.split('/')[1], {type:mime});
  }

  getEvent() {
    setTimeout(() => {
      if (this.data) {
        console.log(this.data)
        this.loadFile = this.data.img ? environment.apiEndpointPublic + this.data.img : null;
        this.angForm.controls['id'].setValue(this.data ? this.data.id : null);
        this.angForm.controls['img'].setValue(this.data ? this.data.img : null);
        this.angForm.controls['name'].setValue(this.data ? this.data.name : null);
        this.angForm.controls['typeEvent'].setValue(this.data ? this.data.typeEvent : null);
        this.angForm.controls['description'].setValue(this.data ? this.data.description : null);
        this.angForm.controls['colour'].setValue(this.data ? this.data.colour : null);
        this.angForm.controls['duration'].setValue(this.data ? this.data.duration : null);
        this.angForm.controls['price'].setValue(this.data ? this.data.price : 0);
        this.angForm.controls['active'].setValue(this.data ? this.data.active : false);
      }
    }, 500);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getEvent();
  }

}
