import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import * as moment from 'moment';
import {MatDialog} from '@angular/material/dialog';
import {DatePipe, TitleCasePipe} from '@angular/common';
import {AvailabilityService} from '../../../../services/availability/availability.service';
import {SnackBarService} from '../../../../services/shared/snackbar.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogComponent} from '../../../../shared/dialog/dialog.component';

@Component({
  selector: 'app-event-form-availability',
  templateUrl: './event-form-availability.component.html',
  styleUrls: ['./event-form-availability.component.css']
})
export class EventFormAvailabilityComponent implements OnChanges {

  @Input() data;
  weekAlways: any[] = [];
  begin: string;
  end: string;
  angForm: FormGroup;

  constructor(private fb: FormBuilder, private snackService: SnackBarService, private dialog: MatDialog, private datePipe: DatePipe, private titlecasePipe: TitleCasePipe, private availabilityService: AvailabilityService) {
    this.begin = this.datePipe.transform(moment('09:00', 'HH:mm'), 'HH:mm').toString();
    this.end = this.datePipe.transform(moment('20:00', 'HH:mm'), 'HH:mm').toString();
    this.weekAlways = [
      {label: 'lunes', day: 1},
      {label: 'martes', day: 2},
      {label: 'miércoles', day: 3},
      {label: 'jueves', day: 4},
      {label: 'viernes', day: 5},
      {label: 'sábado', day: 6},
      {label: 'domingo', day: 0},
    ];
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      availabilities: this.fb.array([])
    });
  }

  get availabilities(): FormArray {
    return this.angForm.get('availabilities') as FormArray;
  }

  addAvailability(availability?: any) {
    const group = this.fb.group(
      {
        id: new FormControl(availability ? availability.id : ''),
        event_id: new FormControl(availability ? availability.event_id : ''),
        date: new FormControl(availability ? availability.date : ''),
        day: new FormControl(availability ? availability.day : '', Validators.required),
        begin: new FormControl(availability ? availability.begin : '', Validators.required),
        end: new FormControl(availability ? availability.end : '', Validators.required),
        always: new FormControl(availability ? availability.always : true),
      });
    this.availabilities.push(group);
  }

  getAvailabilities(availabilities) {
      availabilities.map((a) => {
        a.event_id = this.data.id;
        this.addAvailability(a);
      });
  }

  destroy(i) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Atención',
        content: '¿Estas seguro de eliminarlo?',
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe( result => {
      if(result) {
        this.availabilities.removeAt(i);
      }
    });
  }

  selectDay(control, event) {
    control['day'].setValue(event.toDate().getDay());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.data?.availabilities) {
      this.getAvailabilities(this.data.availabilities);
    } else {
      this.addAvailability();
    }
  }
}
