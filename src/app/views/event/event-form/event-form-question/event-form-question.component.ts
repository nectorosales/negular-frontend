import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SnackBarService} from '../../../../services/shared/snackbar.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from '../../../../shared/dialog/dialog.component';
import {MatSelectChange} from '@angular/material/select';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {GlobalService} from '../../../../services/shared/global.service';

@Component({
  selector: 'app-event-form-questions',
  templateUrl: './event-form-question.component.html',
  styleUrls: ['./event-form-question.component.css']
})
export class EventFormQuestionComponent implements OnChanges {

  @Input() data;
  angForm: FormGroup;
  typesQuestion: any[] = [
    { label: 'Nombre', value: 'name' },
    { label: 'Apellidos', value: 'lastName' },
    { label: 'Email', value: 'email' },
    { label: 'Teléfono', value: 'phone' },
    { label: 'Linea de Texto', value: 'text' },
    { label: 'Area de Texto', value: 'textarea' },
    { label: 'Número', value: 'number' },
    { label: 'Opción Múltiple (CheckBoxes)', value: 'checkbox' },
    { label: 'Opción Única (Radio)', value: 'radio' },
    { label: 'Opción Única (Select)', value: 'select' },
  ];

  constructor(private fb: FormBuilder, private snackService: SnackBarService, private dialog: MatDialog, private globalService: GlobalService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      questions: this.fb.array([])
    });
  }

  get questions(): FormArray {
    return this.angForm.get('questions') as FormArray;
  }

  addQuestion(question?: any, i = 0) {
    const group = this.fb.group(
      {
        id: new FormControl(question ? question.id : ''),
        label: new FormControl(question ? question.label : '', Validators.required),
        required: new FormControl(question ? question.required : false),
        order: new FormControl(question ? question.order : i, Validators.required),
        type: new FormControl(question ? question.type : '', Validators.required),
        options: this.fb.array([])
      });
    if(question) this.getOptions(question, group.controls.options);
    this.questions.push(group);
  }

  addOption(control, option?: any){
    const group = this.fb.group(
      {
        id: new FormControl(option ? option.id : ''),
        label: new FormControl(option ? option.label : ''),
        order: new FormControl(option ? option.order : ''),
      });
    control.push(group);
  }

  getQuestions(questions) {
    questions = this.globalService.sortBy(questions, 'order');
    questions.forEach((a, i) => {
      this.addQuestion(a, i);
    });
  }

  getOptions(question, control) {
    question.options.forEach(a => {
      this.addOption(control, a);
    });

  }

  destroy(i, control) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Atención',
        content: '¿Estas seguro de eliminarlo?',
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe( result => {
      if(result) {
        control ? control.removeAt(i) : this.questions.removeAt(i);
      }
    });
  }

  selectedType(control, event: MatSelectChange) {
    if(event.value === 'radio' || event.value === 'checkbox' || event.value === 'select') {
      if(control.value.length === 0) this.addOption(control);
    } else {
      control.clear();
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    const questions = [];
    event.container.data.forEach((question: any, i) => {
      question.value.order = i;
      questions.push(question.value);
    });
    this.angForm.value.questions = questions;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.data?.questions) {
      this.getQuestions(this.data.questions);
    } else {
      this.getQuestions([{id: '', label: 'Email', type: 'email', required: true, order: 0, options: []}]);
    }
  }
}
