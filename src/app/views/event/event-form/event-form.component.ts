import { Component, OnInit, ViewChild } from '@angular/core';
import { MatVerticalStepper } from '@angular/material/stepper';
import { EventFormDataBasicComponent } from './event-form-data-basic/event-form-data-basic.component';
import { EventFormAvailabilityComponent } from './event-form-availability/event-form-availability.component';
import { EventFormQuestionComponent } from './event-form-question/event-form-question.component';
import { EventService } from '../../../services/event/event.service';
import { UserService } from '../../../services/user/user.service';
import { AvailabilityService } from '../../../services/availability/availability.service';
import { DatePipe } from '@angular/common';
import { QuestionService } from '../../../services/question/question.service';
import { OptionService } from '../../../services/option/option.service';
import { MeetingService } from '../../../services/meeting/meeting.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../services/shared/global.service';
import * as moment from 'moment';
import { TableComponent } from '../../../shared/table/table.component';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {SlugifyPipe} from '../../../pipes/slugify.pipe';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {
  @ViewChild('stepper') public stepper: MatVerticalStepper;
  @ViewChild('formDataBasic') public formDataBasic: EventFormDataBasicComponent;
  @ViewChild('formDataAvailability')
  public formDataAvailability: EventFormAvailabilityComponent;
  @ViewChild('formDataQuestions')
  public formDataQuestions: EventFormQuestionComponent;
  event: any = {};
  load: boolean = false;
  private data: {
    contentDialog: string;
    search: boolean;
    displayColumns: string[];
    data: any[];
    load: boolean;
    columns: (
      | { name: string; label: string; format?: string }
      | { name: string; label: string; type: string }
    )[];
  };
  @ViewChild('tableComponent') tableComponent;

  constructor(
    private globalService: GlobalService,
    private router: Router,
    private route: ActivatedRoute,
    private meetingService: MeetingService,
    private optionService: OptionService,
    private eventService: EventService,
    public userService: UserService,
    private availabilityService: AvailabilityService,
    private datePipe: DatePipe,
    private questionService: QuestionService,
    private dialog: MatDialog,
    private slugifyPipe: SlugifyPipe,
  ) {
    this.tableComponent = TableComponent;
  }

  getMeetings(event) {
    setTimeout(() => {
      this.meetingService.index(event.id).subscribe((meetings: any[]) => {
        meetings = meetings.map(m => {
          m.date = moment(m.start).toDate();
          m.users = m.userMeetings?.length;
          return m;
        });
        this.data = {
          data: meetings,
          search: true,
          columns: [
            { name: 'date', label: 'Fecha', type: 'date', format: 'dd/MM/yy' },
            { name: 'start', label: 'Inicio', type: 'date', format: 'hh:mm a' },
            { name: 'end', label: 'Fín', type: 'date', format: 'hh:mm a' },
            { name: 'users', label: 'Nº Participantes' }
          ],
          load: true,
          displayColumns: ['select', 'date', 'start', 'end', 'users'],
          contentDialog:
            '¿Estas seguro de eliminarlo? <br> Se borraran todas las reuniones asociadas al evento.'
        };
      });
    }, 500);
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      if (param.id !== 'crear') {
        this.eventService.show(param.id).subscribe(event => {
          this.event = event;
          this.load = true;
        });
      } else {
        this.load = true;
      }
      setTimeout(() => {
        this.getMeetings(this.event);
      }, 500);
    });
  }

  onSubmit() {
    if (this.formDataBasic.angForm.value.colour.hex) {
      this.formDataBasic.angForm.value.colour =
        '#' + this.formDataBasic.angForm.value.colour.hex;
    }
    this.formDataBasic.angForm.value.slug = this.slugifyPipe.transform(this.formDataBasic.angForm.value.name);
    if (this.formDataBasic.angForm.value.id) {
      this.eventService
        .update(this.formDataBasic.angForm.value)
        .subscribe((event: any) => {
          this.createOrUpdateAvailabilities(event.id);
          this.createOrUpdateQuestions(event.id);
          this.getMeetingsDrawer(this.userService.user.id);
        });
    } else {
      delete this.formDataBasic.angForm.value.id;
      this.formDataBasic.angForm.value.user = this.userService.user.id;
      this.eventService
        .store(this.formDataBasic.angForm.value)
        .subscribe((event: any) => {
          this.createOrUpdateAvailabilities(event.id);
          this.createOrUpdateQuestions(event.id);
          this.getMeetingsDrawer(this.userService.user.id);
          this.formDataBasic.angForm.controls['id'].setValue(event.id);
        });
      this.router.navigate(['/admin/eventos']);
    }
  }

  getMeetingsDrawer(id) {
    this.meetingService.getMeetingRoomByUser(id).subscribe( (meetings: any[]) => {
      this.meetingService.setMeetings(meetings);
    });
  }

  createOrUpdateQuestions(eventId) {
    if (this.formDataBasic.angForm.value.id) {
      this.deleteQuestionsEvent(eventId);
    }

    this.formDataQuestions.angForm.value.questions.forEach((question, i) => {
      question.order = i;
      const cloneQuestion = Object.assign({}, question);
      if (question.id) {
        if (
          cloneQuestion &&
          cloneQuestion.options &&
          cloneQuestion.options.length > 0
        ) {
          this.createOrUpdateOption(cloneQuestion);
        }
        delete question.event_id;
        delete question.options;
        this.questionService.update(question).subscribe();
      } else {
        delete question.id;
        question.event = eventId;
        this.questionService.store(question).subscribe((q: any) => {
          this.createOrUpdateOption(q);
        });
      }
    });
  }

  createOrUpdateOption(question) {
    if (this.formDataBasic.angForm.value.id) {
      this.deleteOptionsQuestion(question);
    }

    if (question.options.length > 0) {
      question.options.forEach((option, i) => {
        option.order = i;
        if (option.id) {
          delete option.question_id;
          this.optionService.update(option).subscribe();
        } else {
          option.question = question.id;
          delete option.id;
          this.optionService.store(option).subscribe();
        }
      });
    }
  }

  private createOrUpdateAvailabilities(eventId) {
    if (this.formDataBasic.angForm.value.id) {
      this.deleteAvailabilitiesEvent(eventId);
    }

    this.formDataAvailability.angForm.value.availabilities.forEach(
      availability => {
        availability.date = availability.date
          ? this.datePipe.transform(availability.date, 'yyyy-MM-dd').toString()
          : null;
        availability.event = eventId;
        if (availability.id) {
          delete availability.event_id;
          this.availabilityService.update(availability).subscribe();
        } else {
          availability.event = eventId;
          delete availability.id;
          this.availabilityService.store(availability).subscribe();
        }
      }
    );
  }

  private deleteAvailabilitiesEvent(eventId) {
    this.availabilityService
      .index(eventId)
      .subscribe((availabilities: any[]) => {
        var onlyInA = availabilities.filter(
          this.globalService.comparer(
            this.formDataAvailability.angForm.value.availabilities
          )
        );
        var onlyInB = this.formDataAvailability.angForm.value.availabilities.filter(
          this.globalService.comparer(availabilities)
        );
        let result = onlyInA.concat(onlyInB);
        if (result.length > 0) {
          let ids = [];
          result.forEach(availability => {
            if (availability.id) {
              ids.push(availability.id);
            }
          });
          if (ids.length > 0) {
            this.availabilityService.destroySelected(ids).subscribe();
          }
        }
      });
  }

  private deleteQuestionsEvent(eventId) {
    this.questionService.index(eventId).subscribe((questions: any[]) => {
      var onlyInA = questions.filter(
        this.globalService.comparer(
          this.formDataQuestions.angForm.value.questions
        )
      );
      var onlyInB = this.formDataQuestions.angForm.value.questions.filter(
        this.globalService.comparer(questions)
      );
      let result = onlyInA.concat(onlyInB);
      if (result.length > 0) {
        let ids = [];
        result.forEach(question => {
          if (question.id) {
            ids.push(question.id);
          }
        });
        if (ids.length > 0) {
          this.questionService.destroySelected(ids).subscribe();
        }
      }
    });
  }

  private deleteOptionsQuestion(question: any) {
    if (question.options.length > 0) {
      this.optionService.index(question.id).subscribe((options: any[]) => {
        var onlyInA = options.filter(
          this.globalService.comparer(question.options)
        );
        var onlyInB = question.options.filter(
          this.globalService.comparer(options)
        );
        let result = onlyInA.concat(onlyInB);
        if (result.length > 0) {
          let ids = [];
          result.forEach(option => {
            if (option.id) {
              ids.push(option.id);
            }
          });
          if (ids.length > 0) {
            this.optionService.destroySelected(ids).subscribe();
          }
        }
      });
    }
  }

  openForm(meeting: any) {
    this.router.navigate(['/admin/reuniones/' + meeting.id]);
  }

  destroy(selection) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: 'Atención', content: '¿Estas seguro de eliminarlos?<br><small>Se eliminarán los datos de forma permanente</small>' },
      disableClose: true,
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const ids = [];
        selection.forEach(obj => {
          ids.push(obj.id);
        });
        this.meetingService.destroySelected(ids).subscribe( () => {
          this.getMeetings(this.event);
          this.tableComponent.selection.clear();
        });
      }
    });
  }
}
