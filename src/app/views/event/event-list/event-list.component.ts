import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {EventService} from '../../../services/event/event.service';
import {UserService} from '../../../services/user/user.service';
import {EventFormComponent} from '../event-form/event-form.component';
import {TableComponent} from '../../../shared/table/table.component';
import {Event} from "../../../models/event";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  data = { data: [], search: false, columns: [], displayColumns: [], load: false, contentDialog: '' };
  formComponent;
  event: any = {};
  @ViewChild('tableComponent') tableComponent;

  constructor(private dialog: MatDialog, private router: Router, private eventService: EventService, public userService: UserService) {
    this.formComponent = EventFormComponent;
    this.tableComponent = TableComponent;
  }

  ngOnInit() {
    this.getEvents();
  }

  getEvents(){
    setTimeout(() => {
      if(this.userService.user) {
        this.eventService.indexByUser(this.userService.user.id).subscribe((data: any[]) => {
          this.data = {
            data: data,
            search: true,
            columns: [
              { name: 'name', label: 'Nombre' },
              { name: 'colour', label: 'Color', type: 'colour' },
              { name: 'active', label: 'Avtivo', type: 'checkbox' },
              {
                name: 'createdAt',
                label: 'Creado',
                type: 'date'
              }
            ],
            load: true,
            displayColumns: [
              'select',
              'name',
              'colour',
              'active',
              'createdAt',
            ],
            contentDialog: '¿Estas seguro de eliminarlo? <br> Se borraran todas las reuniones asociadas al evento.'
          };

        });
      }
    }, 500);
  }

  openForm(obj = null) {
    if(!obj || obj.event.target.nodeName !== 'DIV') {
      this.router.navigate(['/admin/eventos/', obj ? obj.id : 'crear']);
    }
  }

  destroy(selection) {
    const ids = [];
    selection.forEach(obj => {
      ids.push(obj.id);
    });
    this.eventService.destroySelected(ids).subscribe( () => {
      this.getEvents();
    });
  }

  checkActive(event) {
    console.log(event)
    let obj = {
      id: event.id,
      img: event.img,
      name: event.name,
      typeEvent: event.typeEvent,
      description: event.description,
      colour: event.colour,
      duration: event.duration,
      price: event.price,
      active: event.active
    }
    this.eventService.update(obj).subscribe();
  }
}
