import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {EventListComponent} from './event-list/event-list.component';
import {EventFormComponent} from './event-form/event-form.component';

const routes: Routes = [
  {
    path: '',
    component: EventListComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
  {
    path: ':id',
    component: EventFormComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule {}
