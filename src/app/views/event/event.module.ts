import { NgModule } from '@angular/core';
import {CommonModule, DatePipe, TitleCasePipe} from '@angular/common';
import { EventComponent } from './event.component';
import { EventFormComponent } from './/event-form/event-form.component';
import { EventFormDataBasicComponent } from './/event-form/event-form-data-basic/event-form-data-basic.component';
import {
  MAT_COLOR_FORMATS,
  NgxMatColorPickerModule,
  NGX_MAT_COLOR_FORMATS
} from '@angular-material-components/color-picker';
import { SlugifyPipe } from '../../pipes/slugify.pipe';
import { EventFormAvailabilityComponent } from './/event-form/event-form-availability/event-form-availability.component';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from 'saturn-datepicker';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter
} from '@angular/material-moment-adapter';
import { EventFormQuestionComponent } from './/event-form/event-form-question/event-form-question.component';
import {EventRoutingModule} from './event-routing.module';
import { EventListComponent } from './event-list/event-list.component';
import {SharedModule} from '../../shared/shared.module';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {ReactiveFormsModule} from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgxImageCompressService} from 'ngx-image-compress';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [
    EventFormComponent,
    EventComponent,
    EventFormDataBasicComponent,
    EventFormAvailabilityComponent,
    EventFormQuestionComponent,
    EventListComponent,
  ],
  imports: [
    CommonModule,
    EventRoutingModule,
    SharedModule,
    MatIconModule,
    MatFormFieldModule,
    MatRadioModule,
    ReactiveFormsModule,
    NgxMatColorPickerModule,
    DragDropModule,
    MatStepperModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSlideToggleModule,
  ],
  providers: [
    { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS },
    SlugifyPipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    DatePipe,
    TitleCasePipe,
    NgxImageCompressService
  ]
})
export class EventModule {}
