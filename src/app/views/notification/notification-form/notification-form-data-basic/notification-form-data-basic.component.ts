import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-notification-form-data-basic',
  templateUrl: './notification-form-data-basic.component.html',
  styleUrls: ['./notification-form-data-basic.component.css']
})
export class NotificationFormDataBasicComponent implements OnInit, OnChanges {

  angForm: FormGroup;
  @Input() notification;
  @Input() export: boolean;
  public Editor = ClassicEditor;

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      subject: ['', Validators.required],
      body: ['', [Validators.required, Validators.maxLength(500)]],
    });
  }

  getNotification() {
    setTimeout(() => {
      if (this.notification) {
        this.angForm.controls['id'].setValue(this.notification.id ? this.notification.id : '');
        this.angForm.controls['name'].setValue(this.notification.name ? this.notification.name : '');
        this.angForm.controls['subject'].setValue(this.notification.subject ? this.notification.subject : '');
        this.angForm.controls['body'].setValue(this.notification.body ? this.notification.body : '');
        console.log(this.angForm.value)
      } else {
        this.angForm.reset();
      }
    }, 500);
  }

  ngOnInit() {
    this.getNotification();
  }

  ngOnChanges(changes: SimpleChanges): void {

    this.getNotification();
  }

}
