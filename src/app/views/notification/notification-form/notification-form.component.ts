import {Component, OnInit, ViewChild} from '@angular/core';
import {MatVerticalStepper} from '@angular/material/stepper';
import {MatDialog} from '@angular/material/dialog';
import {MeetingService} from '../../../services/meeting/meeting.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import * as moment from 'moment';
import {DialogComponent} from '../../../shared/dialog/dialog.component';
import {RoomFormDataBasicComponent} from '../../room/room-form/room-form-data-basic/room-form-data-basic.component';
import {NotificationService} from '../../../services/notification/notification.service';

@Component({
  selector: 'app-notification-form',
  templateUrl: './notification-form.component.html',
  styleUrls: ['./notification-form.component.css']
})
export class NotificationFormComponent implements OnInit {

  @ViewChild('stepper') private stepper: MatVerticalStepper;
  @ViewChild('notificationFormDataBasicComponent') notificationFormDataBasicComponent;
  notification: any = {};
  load: boolean = false;
  private data: {
    contentDialog: string;
    search: boolean;
    displayColumns: string[];
    data: any[];
    load: boolean;
    columns: (
      | { name: string; label: string; format?: string }
      | { name: string; label: string; type: string }
      )[];
  };
  @ViewChild('tableComponent') tableComponent;

  constructor(private dialog: MatDialog, private meetingsService: MeetingService, private route: ActivatedRoute, private router: Router, private notificationService: NotificationService, public userService: UserService) {
    this.notificationFormDataBasicComponent = RoomFormDataBasicComponent;
  }

  openForm(meeting: any) {
    this.router.navigate(['/admin/reuniones/' + meeting.id]);
  }

  getMeetings(notification) {
    setTimeout(() => {
      this.meetingsService.indexByRoom(notification.id).subscribe((meetings: any[]) => {
        meetings = meetings.map(m => {
          m.date = moment(m.start).toDate();
          m.users = m.users.length;
          m.name = m.event.name
          return m;
        });
        this.data = {
          data: meetings,
          search: true,
          columns: [
            { name: 'name', label: 'Evento' },
            { name: 'date', label: 'Fecha', type: 'date', format: 'dd/MM/yy' },
            { name: 'start', label: 'Inicio', type: 'date', format: 'hh:mm a' },
            { name: 'end', label: 'Fín', type: 'date', format: 'hh:mm a' },
            { name: 'users', label: 'Nº Participantes' }
          ],
          load: true,
          displayColumns: ['select', 'name', 'date', 'start', 'end', 'users'],
          contentDialog:
            '¿Estas seguro de eliminarlo? <br> Se borraran todas las reuniones asociadas al evento.'
        };
      });
    }, 500);
  }

  nextStep() {
    setTimeout(() => {
      this.stepper.next();
    }, 300);
  }

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      if (param.id !== 'crear') {
        this.notificationService.show(param.id).subscribe(notification => {
          this.notification = notification;
          this.load = true;
        });
      } else {
        this.load = true;
      }
    });
    /*setTimeout(() => {
      this.getMeetings(this.notification);
    }, 500);*/
  }

  submit() {

    if(!this.notification.id) {
      delete this.notificationFormDataBasicComponent.angForm.value.id;
      this.notificationFormDataBasicComponent.angForm.value.user = this.userService.user;
      this.notificationService.store(this.notificationFormDataBasicComponent.angForm.value).subscribe();
    } else {
      this.notificationService.update(this.notificationFormDataBasicComponent.angForm.value).subscribe();
    }
    this.router.navigate(['/admin/notificaciones'])


  }

  destroy(selection) {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { title: 'Atención', content: '¿Estas seguro de eliminarlos?<br><small>Se eliminarán los datos de forma permanente</small>' },
      disableClose: true,
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const ids = [];
        selection.forEach(obj => {
          ids.push(obj.id);
        });
        this.meetingsService.destroySelected(ids).subscribe( () => {
          this.getMeetings(this.notification);
          this.tableComponent.selection.clear();
        });
      }
    });
  }

}
