import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NotificationComponent} from './notification.component';
import {NotificationFormComponent} from './notification-form/notification-form.component';
import {NotificationListComponent} from './notification-list/notification-list.component';
import {NotificationRoutingModule} from './notification-routing.module';
import {MatIconModule} from '@angular/material/icon';
import {SharedModule} from '../../shared/shared.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import { NotificationFormDataBasicComponent } from './notification-form/notification-form-data-basic/notification-form-data-basic.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {MatStepperModule} from '@angular/material/stepper';



@NgModule({
  declarations: [NotificationComponent, NotificationFormComponent, NotificationListComponent, NotificationFormDataBasicComponent],
  exports: [
    NotificationFormDataBasicComponent
  ],
  imports: [
    CommonModule,
    NotificationRoutingModule,
    MatIconModule,
    SharedModule,
    MatTooltipModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    CKEditorModule,
    FormsModule,
    MatStepperModule
  ]
})
export class NotificationModule { }
