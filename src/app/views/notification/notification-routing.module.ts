import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {RoleGuardService} from '../../guards/role-guard.service';
import {Roles} from '../../enums/Roles';
import {AuthGuardService} from '../../guards/auth-guard.service';
import {NotificationListComponent} from './notification-list/notification-list.component';
import {NotificationFormComponent} from './notification-form/notification-form.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationListComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
  {
    path: ':id',
    component: NotificationFormComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    data: { roles: [Roles.USER] },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule {}
