import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {NotificationService} from '../../../services/notification/notification.service';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {

  data = { data: [], search: false, columns: [], displayColumns: [], load: false };

  constructor(private dialog: MatDialog, private router: Router, public notificationService: NotificationService, public userService: UserService) {}

  ngOnInit() {
    this.getNotifications();
  }

  getNotifications(){
    setTimeout(() => {
      if(this.userService.user) {
        this.notificationService.indexByUser(this.userService.user.id).subscribe((data: any[]) => {
          this.data = {
            data: data,
            search: true,
            columns: [
              { name: 'name', label: 'Nombre' },
              {
                name: 'createdAt',
                label: 'Creado',
                type: 'date'
              }
            ],
            load: true,
            displayColumns: [
              'select',
              'name',
              'createdAt',
            ]
          };

        });
      }
    }, 500);
  }

  openForm(obj) {
    console.log(obj)
    this.router.navigate(['/admin/notificaciones/' + obj.id]);
  }


  destroy(selection) {
    const ids = [];
    selection.forEach(obj => {
      ids.push(obj.id);
    });
    this.notificationService.destroySelected(ids).subscribe( () => {
      this.getNotifications();
    });
  }

}
