import {Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import Typed from 'typed.js';
import { DynamicScriptLoaderService } from '../../services/shared/dynamicScriptLoader.service';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {UserService} from '../../services/user/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users: any[] = [];
  angForm: FormGroup;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private renderer: Renderer2,
    private dynamicScriptLoader: DynamicScriptLoaderService,
    private authService: AuthService,
    private router: Router
  ) {
    this.createForm();
  }

  private loadScripts() {
    this.dynamicScriptLoader
      .load('jquery')
      .then(data => {})
      .catch(error => console.log(error));
  }

  createForm() {
    this.angForm = this.fb.group({
      room: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadScripts();
    this.getUsers();
    const options = {
      strings: [
        'Crea tus meetings',
        'Organiza tus reuniones',
        'Selecciona tu disponibilidad',
        'Gestiona tus usuarios',
        'Pagos por Bizum',
        'Notificaciones',
        'Formularios dinámicos',
      ],
      typeSpeed: 40,
      backSpeed: 30,
      showCursor: true,
      cursorChar: '|',
      loop: true
    };

    const typed = new Typed('.typed-element', options);
  }

  getUsers() {
    setTimeout(() => {
      this.userService
        .index()
        .subscribe((users: any[]) => {
          this.users = users.map(user => {
            user.src =  environment.apiEndpointPublic + user.details.img;
            return user;
          });
        });
    }, 800);
  }

  openMeetRoom() {
    window.open(environment.jitsiMeet, '_blank');
  }

  scrollToRegister() {

  }

  signUp(event: any) {
    const obj = {
      name: event.value.name,
      lastName: event.value.lastName,
      phone: event.value.phone,
      email: event.value.email,
      password: event.value.password,
    }
    this.authService.signUp(obj).subscribe( (res: any) => {
      if (res.token) {
        localStorage.setItem('negular-token', res.token);
      }
    });
  }

  openRouteUser(event) {
    if(event.username) this.router.navigate([event.username]);
  }
}
