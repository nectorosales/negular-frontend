# compilar y subir build

Si tienes windows:

- Instala windows terminal y ubuntu lts 20.0.4 en la tienda de windows
- Instala nvm y npm:
  En la consola de windows Terminal y seleccionando la consola de ubuntu, realizar lo siguiente:
  - Accedemos a la home (introduce el comando cd)
  - introduce esta comando: curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
  - Pegamos en el archivo ~/.bash_profile, ~/.zshrc, ~/.profile, or ~/.bashrc según el que se tenga (normalmente sería ~/.bashrc), es decir, abrimos el fichero nano ~/.bashrc
    y pegamos el siguiente comando:
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "\$NVM_DIR/nvm.sh" # This loads nvm
  - Salir de la consola y volver a entrar
  - Instala nvm, nvm install --lts
  - Instalar ng cli: npm install -g @angular/cli

## Después de instalar lo del paso anterior

Desde la carpeta del proyecto front, ejecutar este comando:

- sh deploy/deploy.sh

Si estas trabajando en un sistema windows y falla el script anterior, necesitaras convertir nuevamente el script a formato DOS con esta librería:
- sudo apt update
- sudo apt install dos2unix
- dos2unix deploy.sh


Nos pedirá las credenciales de ssh

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.


# NegularFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
